# GATEWAY UPDATER #

Otomaik connector-converter-config güncelleme scriptleri bulunur.


## kurulum

```
git clone https://bitbucket.org/netcadlabs/gw-updater

cd gw-updater

chmod +x update.sh

sudo > /var/log/thinsboard-gateway/gw-updater.log
sudo chmod +x /var/log/thinsboard-gateway/gw-updater.log
```

## cron job'a ekleme

```
# crontab sudo ile açılır. istenilen console editör seçilir.
sudo crontab -e

# aşağıdaki değer açılan dosyaya eklenir ve kaydedilir.
# 5 dakika da bir kontrol eder.
*/5 * * * *  cd /home/pi/gw-updater && ./update.sh >> /var/log/thinsboard-gateway/gw-updater.log 2>&1
```