#!/bin/bash

source /home/pi/gw-updater/util.sh

Logger "Setting permissions.."

# bağlı usb tara bul ona göre göre yetki ver

DIR="/dev/ttyAMA4"
if [ -d "$DIR" ]; then
  Logger "Giving permission on ${DIR}"
  chmod 777 $DIR
fi

DIR="/dev/ttyACM0"
if [ -d "$DIR" ]; then
  Logger "Giving permission on ${DIR}"
  chmod 777 $DIR
fi
DIR="/dev/ttyACM1"
if [ -d "$DIR" ]; then
  Logger "Giving permission on ${DIR}"
  chmod 777 $DIR
fi
DIR="/dev/ttyACM2"
if [ -d "$DIR" ]; then
  Logger "Giving permission on ${DIR}"
  chmod 777 $DIR
fi
DIR="/dev/ttyACM3"
if [ -d "$DIR" ]; then
  Logger "Giving permission on ${DIR}"
  chmod 777 $DIR
fi

DIR="/dev/gpiomem"
if [ -d "$DIR" ]; then
  Logger "Giving permission on ${DIR}"
  chmod 777 $DIR
fi


chmod a+rwx /var/lib/thingsboard_gateway/extensions/serial/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/gateway/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/labels_map/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/models/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/protos/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/utils/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/transforms/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/ssd/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/nn/*
chmod a+rwx /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/datasets/*
chmod a+rwx /etc/thingsboard-gateway/config/*
chmod a+rwx /usr/lib/python3/dist-packages/thingsboard_gateway/tb_client/*
chmod a+rwx /usr/lib/python3/dist-packages/thingsboard_gateway/gateway/*
chmod a+rwx /etc/thingsboard-gateway/config/tb_gateway.yaml
Logger "Starting copying files..."

# Pratik - işlem yapılacak kaynak/hedef dosyanın var olup olmadığı kontrol edilebilir. 
# FILE=./tb-gate/config/abc.json
# if test -f "$FILE"; then
#     echo "$FILE exist"
# fi

# Pratik - kopyalanacak dosyalar karşılaştırma yapılabilir, kaynak hedef ile aynı ise kopyalanmaz
# if cmp -s "$oldfile" "$newfile" ; then
#    echo "Nothing changed"
# else
#    echo "Something changed"
# fi

cp ./tb-gate/connectors/serial/* /var/lib/thingsboard_gateway/extensions/serial/ -v
cp ./tb-gate/connectors/gateway/* /var/lib/thingsboard_gateway/extensions/gateway/ -v
cp ./tb-gate/connectors/camera/* /var/lib/thingsboard_gateway/extensions/camera/ -v
cp ./tb-gate/connectors/camera/DemoCameraService/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/ -v
cp ./tb-gate/connectors/camera/DemoCameraService/vision/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/ -v
cp ./tb-gate/connectors/camera/DemoCameraService/vision/utils/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/utils/ -v
# cp ./tb-gate/connectors/camera/DemoCameraService/vision/protos/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/protos/ -v
# cp ./tb-gate/connectors/camera/DemoCameraService/vision/transforms/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/transforms/ -v
# cp ./tb-gate/connectors/camera/DemoCameraService/vision/ssd/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/ssd/ -v
# cp ./tb-gate/connectors/camera/DemoCameraService/vision/nn/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/nn/ -v
# cp ./tb-gate/connectors/camera/DemoCameraService/vision/datasets/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/vision/datasets/ -v
# cp ./tb-gate/connectors/camera/DemoCameraService/models/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/models/ -v
# cp ./tb-gate/connectors/camera/DemoCameraService/labels_map/* /var/lib/thingsboard_gateway/extensions/camera/DemoCameraService/labels_map/ -v
cp ./tb-gate/gateway/* /usr/lib/python3/dist-packages/thingsboard_gateway/gateway/ -v
cp ./tb-gate/config/* /etc/thingsboard-gateway/config/ -v

