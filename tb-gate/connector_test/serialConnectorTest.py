import time
import serial
from gpiozero import LED

# serAMA = serial.Serial(
#     port='/dev/ttyUSB0',
#     baudrate = 9600,
#     timeout=0.5
# )
serAMA = serial.Serial(
    port='/dev/ttyAMA4',
    baudrate = 9600,
    timeout=0.5
)
# serAMA = serial.Serial(
#     port='/dev/serial0',
#     baudrate = 9600,
#     timeout=0.5
# )

setHC = LED(17)

def serialComm(command):
    serAMA.write(str.encode(command))
    print(command)
    received_character = b''
    data_from_device = b''
    readCounter = 0
    while received_character != b'\n':  # We will read until receive LF symbol
        try:
            if serAMA is None or not hasattr(serAMA, 'read'):
                # log.debug("NDU - port is not ready..")
                break
            try:
                received_character = serAMA.read(1)    # Read one symbol per time
                if received_character.decode("utf-8") == '':
                    readCounter += 1
                if readCounter > 5:
                    break
            except AttributeError as e:
                print("NDU - AttributeError")
                if serAMA is None:
                    raise e
            except serial.serialutil.SerialException as se:
                print("NDU - SerialException")
                raise se
            else:
                data_from_device = data_from_device + received_character

        except Exception as e:
            time.sleep(1)
            break
    print(str(data_from_device))



try:
    print("set low 1")
    setHC.off()
    time.sleep(1)
    serialComm("AT+DEFAULT\r\n")
    time.sleep(1)
    serialComm("AT+C100\r\n")
    time.sleep(1)
    serialComm("AT+RX\r\n")

    print("set high")
    setHC.on()    
    time.sleep(1)
    while True:
        serialComm("#NDULNDEM01001#SND#CLK1900\r\n")
        time.sleep(1)
except KeyboardInterrupt:
    print("exit")