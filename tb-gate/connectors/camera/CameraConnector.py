"""
Serial olarak gateway'e bağlanmış olan arduino ile bağlantı kurmaya
yarayan örnek connector.

connector ayar dosyası : etc/thingsboard-gateway/config/custom_serial_ardunio_kablo.json
connector'un kullandigi converter (ayar dosyasında belirtilen) :  CustomArduinoSerialConverter (custom_arduino_serial_converter.py)
"""

import time
from threading import Thread
from random import choice
from string import ascii_lowercase
import datetime
import serial
from datetime import datetime, date
from datetime import timezone, timedelta
import requests
import json
import re
import subprocess
from os import path

# Import base class for connector and logger
from thingsboard_gateway.connectors.connector import Connector, log
from thingsboard_gateway.tb_utility.tb_utility import TBUtility
# Import base class for connector and logger

# Define a connector class, it should inherit from "Connector" class.
import sys


workingPath = 'C:\\Program Files\\Python37\\lib\\site-packages\\thingsboard_gateway\\extensions\\camera\\'.replace('\\', path.sep)
sys.path.append(workingPath)

class LuminetCameraConnector(Thread, Connector):
    def __init__(self, gateway, config, connector_type):
        super().__init__()    # Initialize parents classes
        self.statistics = {'MessagesReceived': 0,
                           'MessagesSent': 0}    # Dictionary, will save information about count received and sent messages.
        # Save configuration from the configuration file.
        self.__config = config
        log.debug("NDU - config %s", config)
        # Save gateway object, we will use some gateway methods for adding devices and saving data from them.
        self.__gateway = gateway
        # get from the configuration or create name for logs.
        self.setName(self.__config.get("name", "Custom %s connector " % self.get_name() + ''.join(choice(ascii_lowercase) for _ in range(5))))
        # Send message to logger
        log.info("Starting Custom %s connector", self.get_name())
        self.daemon = True    # Set self thread as daemon
        self.stopped = True    # Service variable for check state
        self.__connected = False    # Service variable for check connection to device
        # Dictionary with devices, will contain devices configurations, converters for devices and serial port objects
        self.__devices = {}
        self.__masterCameraName = self.__config.get("devices")[0].get("name")
        self.__load_converters(connector_type)
        # Call function to load converters and save it into devices dictionary
        self.lastConnectionCheck = 0
        # first interval(seconds) to reconnect device
        self.reconnect_interval = 5
        self.reconnect_count = 0  # number of reconnection attempt
        self.__connect_to_devices()    # Call function for connect to devices
        self.__waitSession = False
        self.deviceLastAttributes = {}
        self.__setAttributeQuery = {}
        for device in config.get("devices"):
            self.deviceLastAttributes[device.get("name")] = {}
            self.__setAttributeQuery[device.get("name")] = [""]

        log.info('Custom connector %s initialization success.',
                 self.get_name())    # Message to logger
        log.info("Devices in configuration file found: %s ", '\n'.join(
            device for device in self.__devices))    # Message to logger
        
        self.frame_rate_calc = 0
        self.imageHeight = 480
        self.imageWidth = 640
        self.streaming = True
        log.debug('NDU - found person %s', self.frame_rate_calc)
        log.debug('NDU - found person %s', self.imageHeight)
        log.debug('NDU - found person %s', self.imageWidth)
        log.debug('NDU - found person %s', self.streaming)

    # Function for opening connection and connecting to devices
    def __connect_to_devices(self):
        # @warn camera must open there
        device_config = self.__devices[self.__masterCameraName]['device_config']
        log.debug("NDU - camera connection established for %s",
                  device_config["name"])
        self.__gateway.add_device(device_config["name"], {
                                  "connector": self}, device_type=device_config["type"])
        self.__connected = True
        self.lastConnectionCheck = time.time()

    def __load_converters(self, connector_type):
        devices_config = self.__config.get('devices')
        try:
            if devices_config is not None:
                for device_config in devices_config:
                    if device_config.get('converter') is not None:
                        converter = TBUtility.check_and_import(
                            connector_type, device_config['converter'])
                        self.__devices[device_config['name']] = {'converter': converter(device_config),
                                                                 'device_config': device_config}
                        log.debug("NDU - __load_converters converter %s loaded with : %s",
                                  device_config['converter'], device_config)
                    else:
                        log.error(
                            'Converter configuration for the custom connector %s -- not found, please check your configuration file.', self.get_name())
            else:
                log.error(
                    'Section "devices" in the configuration not found. A custom connector %s has being stopped.', self.get_name())
                self.close()
        except Exception as e:
            log.exception(e)

    def open(self):    # Function called by gateway on start
        self.stopped = False
        self.start()

    def get_name(self):    # Function used for logging, sending data and statistic
        return self.name

    def is_connected(self):    # Function for checking connection state
        return self.__connected

    def getCameraResult(self, currentDevice):
        try:
            #@warn readstreamFile
            telemList = []
            with open(workingPath +'serviceTelemetry.txt'.replace('/', path.sep), 'r') as telemetryFile:
            # with open('/var/lib/thingsboard_gateway/extensions/camera/serviceTelemetry.txt', 'r') as telemetryFile:
                lines = list(telemetryFile.readlines())
                if len(list(lines)) > 9:
                    for line in lines:
                        telemList.append(line)
                else:
                    return
                #log.debug("NDU Camera Connector %s ", str(personcount))
            
            #log.debug("NDU Camera Connector loop")
            currentConfig = self.__devices[currentDevice]['device_config']
            #log.debug('NDU - found person %s', currentConfig)

            result_dict = {
                'deviceName': currentConfig.get('name', 'CustomSerialDevice'),
                'deviceType': currentConfig.get('deviceType', 'default'),
                'attributes': [],
                'telemetry': [],
            }

            # log.info("NDU - len list : %s telemlist : %s ", str(len(telemList)), str(telemList))
            if len(telemList) > 0 :
                result_dict['telemetry'].append({"PERSON": telemList[0]})
                result_dict['telemetry'].append({"CAR": telemList[1]})
                result_dict['telemetry'].append({"SOCIAL DISTANCE VIOLATION": telemList[2]})
                result_dict['telemetry'].append({"ANGRY": telemList[3]})
                result_dict['telemetry'].append({"DISGUST": telemList[4]})
                result_dict['telemetry'].append({"FEAR": telemList[5]})
                result_dict['telemetry'].append({"HAPPY": telemList[6]})
                result_dict['telemetry'].append({"SAD": telemList[7]})
                result_dict['telemetry'].append({"SUPRISE": telemList[8]})
                result_dict['telemetry'].append({"NEUTRAL": telemList[9]})
                result_dict['telemetry'].append({"MASK OKEY": telemList[10]})
                result_dict['telemetry'].append({"MASK WARNING": telemList[11]})
                result_dict['telemetry'].append({"FRAME_RATE": telemList[12]})
                self.__gateway.send_to_storage(self.get_name(), result_dict)
        except Exception as e:
            log.debug("NDU Camera Connector Failed")
            self.log_exception(e)

    def run(self):    # Main loop of thread
        # self.setcameraconfig
        # self.setcameramodel
        try:
            while True:
                # @warn read serial connections
                self.getCameraResult(self.__masterCameraName)
                time.sleep(0.1)
        except Exception as e:
            self.log_exception(e)

    def log_exception(self, e):
        if hasattr(e, 'message'):
            log.error(e.message)
        else:
            log.exception(e)

    def close(self):    # Close connect function, usually used if exception handled in gateway main loop or in connector main loop
        # @warn
        self.stopped = True
        self.__gateway.del_device(self.__devices[self.__masterCameraName])

    # Function used for processing attribute update requests from ThingsBoard
    def on_attributes_update(self, content):
        device_name = content["device"]
        log.debug(
            "NDU - on_attributes_update device : %s , content : %s", content, device_name)
        if self.__devices.get(device_name) is not None:
            # log.debug("NDU - device : %s", self.__devices.get(device_name))
            device_config = self.__devices[device_name].get("device_config")
            if device_config is not None:
                # log.debug("NDU - device_config %s", device_config)
                if device_config.get("attributeUpdates") is not None:
                    requests = device_config["attributeUpdates"]
                    for request in requests:
                        attribute = request.get("name")
                        # sadece config icerisinde olan attribute isimleri cihaza gonderiliyor
                        if attribute is not None and attribute in content["data"]:
                            # log.debug("NDU - gelen attribute : %s", attribute)
                            try:
                                value = content["data"][attribute]
                                self.deviceLastAttributes[device_name][attribute] = value
                                # @warn saat eklenecek
                                # @warn string kontrol edilecek
                                time.sleep(.01)
                            except Exception as e:
                                self.log_exception(e)

    def server_side_rpc_handler(self, content):
        # log.info("RPC KOMUT GELDİ %s",content)
        if content is not None and content["device"] is not None:
            if content["data"] is None or content["data"].get("method") is None:
                log.debug(
                    "NDU - rpc data or method not found in content : %s", content)
                pass

            device_name = content["device"]
            data = content["data"]
            method = content["data"].get("method")
            # log.debug("NDU - rpc method : %s", method)
            # log.debug("NDU - rpc data : %s", data)

            if self.__devices.get(device_name) is not None:
                device_config = self.__devices[device_name].get(
                    "device_config")
                if device_config is not None and device_config.get("capabilities") is not None:
                    capabilities = device_config.get("capabilities")
                    found = False
                    for capability in capabilities:
                        if method == capability["method"]:
                            self.run_capability(device_name, capability, data)
                            found = True
                    if not found:
                        log.debug("NDU - capability not found : %s", method)
                else:
                    log.debug("NDU - device capabilities not found.")

    # data > {'id': 28, 'method': 'setLedStatus', 'params': {'status': 0}}
    # capability > {'method': 'setLedStatus', 'description': 'Set led status', 'stringToDevice': 'L1=${status}\n', 'singleParams': False, 'parameters': [{'name': 'status', 'type': 'number'}]}
    def run_capability(self, device_name, capability, data):
        try:
            stringToDevice = capability["stringToDevice"]  # check if exist

            log.debug("NDU - capability will run : %s", capability)
            if capability.get("parameters", None) is None:
                if capability.get("singleParams", False):
                    # log.debug("NDU - running singleparams capability")
                    stringToDevice = stringToDevice.replace(
                        "${" + "params" + "}", str(data["params"]))
                # elif capability.get("singleParams", True): # do nothing
                #     log.debug("NDU - running no paramter capability")
            else:
                for capability_param in capability["parameters"]:
                    for param in data["params"]:
                        # log.debug("NDU - gelen param : %s, capability_param.name : %s", param, capability_param["name"])
                        # log.debug("NDU - gelen type(param) : %s, type(capability_param.name) : %s", type(param), type(capability_param["name"]))
                        if str(capability_param["name"]) == str(param):
                            replace_expression = "${" + \
                                capability_param["name"] + "}"
                            log.debug("NDU - replace_expression %s with %s",
                                      replace_expression, str(data["params"].get(param)))
                            stringToDevice = stringToDevice.replace(
                                replace_expression, str(data["params"].get(param)))

            # log.debug("NDU - stringToDevice : %s", stringToDevice)
            # checksum eklenecek
            str_to_send = str(stringToDevice).encode("UTF-8")
            self.__devices[device_name]["serial"].write(str_to_send)

        except Exception as e:
            self.log_exception(e)
