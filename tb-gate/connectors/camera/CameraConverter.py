from thingsboard_gateway.connectors.converter import Converter, log

import random
import psutil

class LuminetCameraConverter(Converter):
    def __init__(self, config):
        self.__config = config
        self.result_dict = {
            'deviceName': config.get('name', 'CustomSerialDevice'),
            'deviceType': config.get('deviceType', 'default'),
            'attributes': [],
            'telemetry': []
        }

    def convert(self, config, data: bytes):
        keys = ['attributes', 'telemetry']
        for key in keys:
            self.result_dict[key] = []
            if self.__config.get(key) is not None:
                for config_object in self.__config.get(key):

                    value = round(random.uniform(20, 40), 0)

                    if str(config_object['key']) == str("CPU"):
                        value = psutil.cpu_percent() * 2

                    if str(config_object['key']) == str("MEMORY"):
                        value = psutil.virtual_memory()[2] * 2

                    if str(config_object['key']) == str("temp"):
                        value = round(random.uniform(40, 50), 3)

                    if str(config_object['key']) == str("humidity"):
                        value = round(random.uniform(10, 20), 4)
                    
                    converted_data = {config_object['key']: value}

                    self.result_dict[key].append(converted_data)

        log.debug("Converted data: %s", self.result_dict)

        self.result_dict['telemetry'].append(converted_data)
        return self.result_dict