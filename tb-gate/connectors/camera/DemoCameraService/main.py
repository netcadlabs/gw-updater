import cv2
import os
import argparse
from network_model import model
from aux_functions import *
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import sys
from keras.models import model_from_json
from keras.preprocessing.image import img_to_array
from keras.models import load_model
from keras.preprocessing.image import load_img
from keras.applications import imagenet_utils
from driverModel import create_model
import numpy as np
import operator
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input

workingPath = '/var/lib/thingsboard_gateway/extensions/camera/'
sys.path.append(workingPath)

# Suppress TF warnings
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

mouse_pts = []

def get_mouse_points(event, x, y, flags, param):
    # Used to mark 4 points on the frame zero of the video that will be warped
    # Used to mark 2 points on the frame zero of the video that are 6 feet away
    global mouseX, mouseY, mouse_pts
    if event == cv2.EVENT_LBUTTONDOWN:
        mouseX, mouseY = x, y
        cv2.circle(image, (x, y), 10, (0, 255, 255), 10)
        if "mouse_pts" not in globals():
            mouse_pts = []
        mouse_pts.append((x, y))
        print("Point detected")
        print(mouse_pts)
        
def writeTelemetryFile(telemetryArray):
        with open(workingPath + 'serviceTelemetry.txt', 'r+') as f:
            f.seek(0)
            for i in range(len(telemetryArray)):
                f.write(str(telemetryArray[i]) + '\n')
            f.truncate()
            f.close()

        with open(workingPath + 'serviceTelemetry.txt', 'r') as myfile:
            personcount = myfile.read()
            print("person count: %s ", str(telemetryArray))
            
def readAttributeFile():
    return

def getMaskStateAnalysis(face_frame):
    try: 
        face_frame = cv2.cvtColor(face_frame, cv2.COLOR_BGR2RGB)
        face_frame = cv2.resize(face_frame, (224, 224))
        face_frame = img_to_array(face_frame)
        face_frame = np.expand_dims(face_frame, axis=0)
        face_frame =  preprocess_input(face_frame)
        preds = mask_model.predict(face_frame)
        for pred in preds:
            (mask, withoutMask) = pred
        label = ""
        if mask > withoutMask:
            label = "Mask"
            maskState_sum[0] += 1
        else:
            maskState_sum[1] += 1 
            label = "No Mask"
        color = (0, 255, 0) if label == "Mask" else (0, 0, 255)
        label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)

    except Exception as e:
        print("mask detection error")
        print(e)

def getEmotionAnalysis(img):    
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    try:
        faces = face_cascade.detectMultiScale(gray, 1.3, 5)
        if len(faces) < 1: 
            emotions_sum[len(emotions_sum) - 1] += 1
            return

        for (x, y, w, h) in faces:
            cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
            detected_face = img[int(y):int(y+h), int(x):int(x+w)]
            getMaskStateAnalysis(detected_face)
            detected_face = cv2.cvtColor(detected_face, cv2.COLOR_BGR2GRAY)  # transform to gray scale
            detected_face = cv2.resize(detected_face, (48, 48))  # resize to 48x48

            img_pixels = img_to_array(detected_face)
            img_pixels = np.expand_dims(img_pixels, axis=0)
            img_pixels /= 255
            emotion_predictions = emotions_model.predict(img_pixels)
            max_index = np.argmax(emotion_predictions[0])
            emotion = emotions[max_index]
            emotions_sum[max_index] += 1
            cv2.putText(img, emotion, (int(x), int(y)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)

    except Exception as e:
        print("emotion detection error")
        print(e)
        emotions_sum[len(emotions_sum) - 1] += 1

# def getDriverAnalysis(frame, driverAnalysis_sum):
#     try:
#         resImg = cv2.resize(frame, (150, 150)) 
#         image_arr = img_to_array(resImg)
#         image_arr = np.expand_dims(image_arr, axis=0)
#         image_arr /= 255

#         # classify given an image
#         predictions = _driverModel.predict(image_arr)

#         # get human-readable labels of the preditions, as well as the corresponding probability
#         decoded_predictions = dict(zip(driverClassLabels, predictions[0]))

#         # sort dictionary by value
#         decoded_predictions = sorted(decoded_predictions.items(), key=operator.itemgetter(1), reverse=True)

#         print("predictions %s", decoded_predictions)
#         count = 1
#         for key, value in decoded_predictions[:5]:
#             print("{}. {}: {:8f}%".format(count, key, value*100))
#             count+=1
#             # print image
#         cv2.imshow(resImg)
#         return driverAnalysis_sum
#     except Exception as e:
#         print("prediction failed")
#         print(e)
#         return driverAnalysis_sum
    
input_video = workingPath + 'video4.mp4'
# driverClassLabels = ['safe_driving', 'texting_right', 'talking_on_phone_right', 'texting_left', 'talking_on_phone_left',
#                 'operating_radio', 'drinking', 'reaching_behind', 'doing_hair_makeup', 'talking_to_passanger']
    
# _driverModel = create_model()
# _driverModel.load_weights("DriverWeights.h5")
# _driverModel.compile(loss='categorical_crossentropy',
#               optimizer='rmsprop',
#               metrics=['accuracy'])

DNN = model()
# Get video handle
frameWidth = 640
frameHeight = 480
camera = PiCamera()
camera.resolution = (frameWidth, frameHeight)
camera.framerate = 32
camera.rotation = 180
rawCapture = PiRGBArray(camera, size=(frameWidth, frameHeight))
fps = 32
width = frameWidth
height = frameHeight
scale_w = 1.2 / 2
scale_h = 1.2 / 2

SOLID_BACK_COLOR = (41, 41, 41)
# Setuo video writer
fourcc = cv2.VideoWriter_fourcc(*"XVID")
output_movie = cv2.VideoWriter("Pedestrian_detect.avi", fourcc, fps, (width, height))
bird_movie = cv2.VideoWriter( "Pedestrian_bird.avi", fourcc, fps, (int(width * scale_w), int(height * scale_h)))
# Initialize necessary variables
frame_num = 0
total_pedestrians_detected = 0
total_six_feet_violations = 0
total_pairs = 0
abs_six_feet_violations = 0
pedestrian_per_sec = 0
sh_index = 1
sc_index = 1

face_cascade = cv2.CascadeClassifier('HCFF.xml')
emotions_model = model_from_json(open("FEMS.json", "r").read())
emotions_model.load_weights('FEMW.h5')  # load weights
mask_model = load_model('mask.h5')  # load weights
emotions = ('angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral')
maskStates = ('untroubled', 'mask warning')

cv2.namedWindow("image")
cv2.setMouseCallback("image", get_mouse_points)
num_mouse_points = 0
first_frame_display = True

# Process each frame, until end of video
for frameFromCam in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
    try:
        frame = np.copy(frameFromCam.array)
        frame_num += 1

        frame_h = frame.shape[0]
        frame_w = frame.shape[1]

        if frame_num == 1:
            # Ask user to mark parallel points and two points 6 feet apart. Order bl, br, tr, tl, p1, p2
            while True:
                image = frame
                cv2.imshow("image", image)
                cv2.waitKey(1)
                if len(mouse_pts) == 7:
                    cv2.destroyWindow("image")
                    break
                first_frame_display = False
            four_points = mouse_pts

            # Get perspective
            M, Minv = get_camera_perspective(frame, four_points[0:4])
            pts = src = np.float32(np.array([four_points[4:]]))
            warped_pt = cv2.perspectiveTransform(pts, M)[0]
            d_thresh = np.sqrt(
                (warped_pt[0][0] - warped_pt[1][0]) ** 2
                + (warped_pt[0][1] - warped_pt[1][1]) ** 2
            )
            bird_image = np.zeros(
                (int(frame_h * scale_h), int(frame_w * scale_w), 3), np.uint8
            )

            bird_image[:] = SOLID_BACK_COLOR
            pedestrian_detect = frame

        print("Processing frame: ", frame_num)

        # draw polygon of ROI
        pts = np.array([four_points[0], four_points[1], four_points[3], four_points[2]], np.int32)
        cv2.polylines(frame, [pts], True, (0, 255, 255), thickness=4)
        
        six_feet_violations = 0
        pedestrian_boxes = 0
        try:
            pedestrian_boxes, num_pedestrians, imageList = DNN.detect_pedestrians(frame)
            print(pedestrian_boxes)
            emotions_sum = [0,0,0,0,0,0,0]
            maskState_sum = [0,0]
            driverAnalysis_sum = [0,0,0,0,0,0,0,0,0,0]
            if len(imageList) > 0:
                for person in imageList:            
                    getEmotionAnalysis(person)
                    # try:
                    #     driverAnalysis_sum = getDriverAnalysis(person, driverAnalysis_sum)
                    # except Exception as e:
                    #     print("driver model exception")
                    #     print(e)
                    #     continue 
        except Exception as e:
            print("person model exception")
            print(e)
            continue 

        
        if len(pedestrian_boxes) > 0:
            pedestrian_detect = plot_pedestrian_boxes_on_image(frame, pedestrian_boxes)
            warped_pts, bird_image = plot_points_on_bird_eye_view(frame, pedestrian_boxes, M, scale_w, scale_h)
            six_feet_violations, ten_feet_violations, pairs = plot_lines_between_nodes(warped_pts, bird_image, d_thresh)
            total_pedestrians_detected += num_pedestrians
            total_pairs += pairs
            abs_six_feet_violations += six_feet_violations
            pedestrian_per_sec, sh_index = calculate_stay_at_home_index(total_pedestrians_detected, frame_num, fps)

        last_h = 75
        text = "# 6ft violations: " + str(int(total_six_feet_violations))
        pedestrian_detect, last_h = put_text(pedestrian_detect, text, text_offset_y=last_h)

        telemetryArray = [str(len(pedestrian_boxes)), str(0), str(six_feet_violations)]
        for emotion in emotions_sum:
            telemetryArray.append(str(emotion))
        for maskstate in maskState_sum:
            telemetryArray.append(str(maskstate))
        print("telemetry array length %s ", len(telemetryArray))
        
        writeTelemetryFile(telemetryArray)
        cv2.imshow("Street Cam", frame)
        
        rawCapture.truncate(0)
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"):
            cv2.destroyAllWindows()
            rawCapture.truncate(0)
            camera.close()
            break
    except KeyboardInterrupt:
        rawCapture.truncate(0)
        camera.close()
        cv2.destroyAllWindows()
        print("exit")
        break

camera.close()
