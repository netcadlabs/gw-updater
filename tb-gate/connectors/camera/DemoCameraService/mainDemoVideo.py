import cv2
import os
import argparse
from vision.utils.network_model import ObjectDetectionModel
from vision.utils.aux_functions import *
import vision.utils.EmotionModel as EmotionModel
# from picamera.array import PiRGBArray
# from picamera import PiCamera
import time
import sys
# from keras.models import model_from_json
# from keras.preprocessing.image import img_to_array
# from keras.models import load_model
# from keras.preprocessing.image import load_img
# from keras.applications import imagenet_utils
# from driverModel import create_model
import numpy as np
import operator
from keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.preprocessing.image import img_to_array
from tensorflow.keras.models import load_model
from tensorflow.keras.applications.mobilenet_v2 import preprocess_input
import json

# face_detector2 onnx model
import onnx
from caffe2.python.onnx import backend

import onnxruntime as ort
import vision.utils.box_utils

# face_detector3 ssd model
from vision.ssd.mb_tiny_fd import create_mb_tiny_fd, create_mb_tiny_fd_predictor
from vision.ssd.mb_tiny_RFB_fd import create_Mb_Tiny_RFB_fd, create_Mb_Tiny_RFB_fd_predictor
from vision.utils.misc import Timer
from vision.ssd.config.fd_config import define_img_size

# workingPath = '/var/lib/thingsboard_gateway/extensions/camera/'
# homePath = workingPath + "DemoCameraService/"
# modelPath = homePath + "models/"
# labelPath = homePath + "labels_map/"
# mediaPath = workingPath + "videos/"

workingPath = 'D:\\gw-updater\\tb-gate\\connectors\\camera\\'
pythonPath = 'C:\\Program Files\\Python37\Lib\\site-packages\\thingsboard_gateway\\extensions\\camera\\'
homePath = workingPath + "DemoCameraService\\"
modelPath = homePath + "models\\"
labelPath = homePath + "labels_map\\"
mediaPath = workingPath + "videos\\"

sys.path.append(workingPath)

# faceModelFile = "/root/.deepface/weights/res10_300x300_ssd_iter_140000.caffemodel"
# faceConfigFile = "/root/.deepface/weights/deploy.prototxt"
# faceModelFile = "C:\\Users\\erkut-msi\\.deepface\\weights\\res10_300x300_ssd_iter_140000.caffemodel"
# faceConfigFile = "C:\\Users\\erkut-msi\\.deepface\\weights\\deploy.prototxt"

# label_path = labelPath + "faceModelLabel.txt"
# onnx_path = modelPath + "version-RFB-640.onnx"

faceLabelPath = labelPath + "voc_FaceLabel.txt"
faceModelPath = modelPath + "version-RFB-640.pth"

personLabelPath = labelPath + "mscoco_label_map.pbtxt"
personModelPath = modelPath + "frozen_inference_graph.pb" 

maskModelPath = modelPath + "mask.h5" 

personConfidence = 0.8
faceConfidence = 0.9
maskConfidence = 0.001

isEmotionDetection = True
isMaskDetection = False
isFaceDetecion = True
isPersonDetection = False
isVehicleDetection = False
isSocialDistance = False
isCrowdCounting = False

#facenet
face_class_names = [name.strip() for name in open(faceLabelPath).readlines()]
define_img_size(480)
test_device = "cpu"
candidate_size = 1000

# #personNet
# person_class_names = [name.strip() for name in open(personLabelPath).readlines()]
# define_img_size(480)
# person_test_device = "cpu"
# person_candidate_size = 1000

# input_video = mediaPath + 'sokak_720.mp4'
# input_video = mediaPath + 'bekleme_720.mp4'
# input_video = mediaPath + 'kasa_720.mp4'
input_video = mediaPath + 'emotion_720.mp4'
# input_video = mediaPath + 'meydan_720.mp4'
# input_video = mediaPath + 'abide_720.mp4'
# input_video = mediaPath + 'traffic_720.mp4'
# input_video = mediaPath + 'gate_720.mp4'
# input_video = mediaPath + 'crowded_720.mp4'

# Suppress TF warnings
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"

mouse_pts = []

# load the face mask detector model from disk

print("[INFO] loading face detector model...")
cocoDNN = ObjectDetectionModel(personModelPath, personLabelPath)

faceNetModel = create_Mb_Tiny_RFB_fd(len(face_class_names), is_test=True, device=test_device)
facePretictor = create_Mb_Tiny_RFB_fd_predictor(faceNetModel, candidate_size=candidate_size, device=test_device)
faceNetModel.load(faceModelPath)

# print("[INFO] loading person detector model...")
# personNetModel = create_Mb_Tiny_RFB_fd(len(person_class_names), is_test=True, device=person_test_device)
# personPretictor = create_Mb_Tiny_RFB_fd_predictor(personNetModel, candidate_size=person_candidate_size, device=person_test_device)
# personNetModel.init_from_pretrained_ssd(personModelPath)

# face_detector2 onnx
# predictor = onnx.load(onnx_path)
# onnx.checker.check_model(predictor)
# onnx.helper.printable_graph(predictor.graph)
# predictor = backend.prepare(predictor, device="CPU")  # default CPU
# ort_session = ort.InferenceSession(onnx_path)
# input_name = ort_session.get_inputs()[0].name

print("[INFO] loading mask detector model...")
mask_model = load_model(maskModelPath)  # load weights
maskStates = ('untroubled', 'mask warning')

print("[INFO] loading emotion detector model...")
emotion_model = EmotionModel.loadModel()
emotion_labels = ['angry', 'disgust', 'fear', 'happy', 'sad', 'surprise', 'neutral']


def get_mouse_points(event, x, y, flags, param):
    # Used to mark 4 points on the frame zero of the video that will be warped
    # Used to mark 2 points on the frame zero of the video that are 6 feet away
    global mouseX, mouseY, mouse_pts
    if event == cv2.EVENT_LBUTTONDOWN:
        mouseX, mouseY = x, y
        cv2.circle(image, (x, y), 10, (0, 255, 255), 10)
        if "mouse_pts" not in globals():
            mouse_pts = []
        mouse_pts.append((x, y))
        print("Point detected")
        print(mouse_pts)

def writeTelemetryFile(telemetryArray):
    with open(pythonPath + 'serviceTelemetry.txt', 'r+') as f:
        f.seek(0)
        for i in range(len(telemetryArray)):
            f.write(str(telemetryArray[i]) + '\n')
        f.truncate()
        f.close()

    with open(workingPath + 'serviceTelemetry.txt', 'r') as myfile:
        personcount = myfile.read()
        print("person count: %s ", str(telemetryArray))

def readAttributeFile():
    return

def image_resize(image, width = None, height = None, inter = cv2.INTER_AREA):
    dim = None
    (h, w) = image.shape[:2]

    if width is None and height is None:
        return image

    if width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    else:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv2.resize(image, dim, interpolation = inter)

    return resized

def face_predict(width, height, confidences, boxes, prob_threshold, iou_threshold=0.3, top_k=-1):
    boxes = boxes[0]
    confidences = confidences[0]
    picked_box_probs = []
    picked_labels = []
    for class_index in range(1, confidences.shape[1]):
        probs = confidences[:, class_index]
        mask = probs > prob_threshold
        probs = probs[mask]
        if probs.shape[0] == 0:
            continue
        subset_boxes = boxes[mask, :]
        box_probs = np.concatenate([subset_boxes, probs.reshape(-1, 1)], axis=1)
        box_probs = box_utils.hard_nms(box_probs, iou_threshold=iou_threshold, top_k=top_k)
        picked_box_probs.append(box_probs)
        picked_labels.extend([class_index] * box_probs.shape[0])
    if not picked_box_probs:
        return np.array([]), np.array([]), np.array([])
    picked_box_probs = np.concatenate(picked_box_probs)
    picked_box_probs[:, 0] *= width
    picked_box_probs[:, 1] *= height
    picked_box_probs[:, 2] *= width
    picked_box_probs[:, 3] *= height
    return picked_box_probs[:, :4].astype(np.int32), np.array(picked_labels), picked_box_probs[:, 4]

def face_detector3(orig_image, personCounter):
    time_face = time.time()
    totalMaskDetectTime = 0
    totalEmotionDetectTime = 0
    totalLoopTime = 0

    (h, w) = orig_image.shape[:2]
    image = cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)
    boxes, labels, probs = facePretictor.predict(image, candidate_size / 2, faceConfidence)
    faceCounter = 0

    for i in range(boxes.size(0)):
        time_loop = time.time()
        box = boxes[i, :]
        (startX, startY) = (max(0, int(box[0]) - 10), max(0, int(box[1]) - 10))
        (endX, endY) = (min(w - 1, int(box[2]) + 10), min(h - 1, int(box[3]) + 10))
        label = ""
        color1 = (0, 0, 255)
        if isEmotionDetection:
            time_emotion = time.time()
            label2, color2 = getEmotionsAnalysis2(orig_image[startY:endY,startX:endX])
            label += " " + label2
            # cv2.rectangle(orig_image, (startX + 4, startY + 4), (endX - 4, endY - 4), color2, 2)
            totalEmotionDetectTime += (time.time() - time_emotion)

        if isMaskDetection:
            time_mask = time.time()
            label1, color1, maskstate = getMaskStateAnalysis(orig_image[startY:endY,startX:endX])
            if not maskstate:
                label += " " + label1
                cv2.rectangle(orig_image, (startX + 2, startY + 2), (endX - 2, endY - 2), color1, 1)
            totalMaskDetectTime += (time.time() - time_mask)
        
        cv2.putText(orig_image, label, (endX - 3, endY - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color1, 2)
        # cv2.rectangle(orig_image, (startX, startY), (endX, endY), (255, 255, 0), 1)
        faceCounter += 1 
        totalLoopTime += time.time() - time_loop
    orig_image = cv2.resize(orig_image, (0, 0), fx=1, fy=1)

    # print("mask detect time:{}".format(totalMaskDetectTime))
    # print("emot detect time:{}".format(totalEmotionDetectTime))
    # print("total loop time:{}".format(totalLoopTime))
    print("total face time:{}".format(time.time() - time_face))

    if personCounter > faceCounter:    
        return personCounter
    else:
        return faceCounter

def person_detector3(orig_image, personCounter):
    (h, w) = orig_image.shape[:2]
    image = orig_image.copy() #cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)
    boxes, labels, probs = personPretictor.predict(image, person_candidate_size / 2, personConfidence)
    faceCounter = 0
    print(str(boxes))
    for i in range(boxes.size(0)):
        time_loop = time.time()
        box = boxes[i, :]
        (startX, startY) = (max(0, int(box[0]) - 10), max(0, int(box[1]) - 10))
        (endX, endY) = (min(w - 1, int(box[2]) + 10), min(h - 1, int(box[3]) + 10))
        label = "vehicle"
        color1 = (0, 0, 255)
        cv2.putText(orig_image, label, (startX - 3, startY - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color1, 2)
        cv2.rectangle(orig_image, (startX, startY), (endX, endY), (255, 255, 0), 1)
        # totalLoopTime += time.time() - time_loop

    orig_image = cv2.resize(orig_image, (0, 0), fx=1, fy=1)
    return boxes.size(0)

def face_detector2(orig_image):
    time_time = time.time()
    threshold = 0.8
    (h, w) = orig_image.shape[:2]
    image = cv2.cvtColor(orig_image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (640, 480))
    image_mean = np.array([127, 127, 127])
    image = (image - image_mean) / 128
    image = np.transpose(image, [2, 0, 1])
    image = np.expand_dims(image, axis=0)
    image = image.astype(np.float32)
    confidences, boxes = predictor.run(image)
    confidences, boxes = ort_session.run(None, {input_name: image})
    boxes, labels, probs = face_predict(orig_image.shape[1], orig_image.shape[0], confidences, boxes, threshold)
    
    for i in range(boxes.shape[0]):
        box = boxes[i, :]
        (startX, startY) = (max(0, box[0] - 10), max(0, box[1] - 10))
        (endX, endY) = (min(w - 1, box[2] + 10), min(h - 1, box[3] + 10))

        # label = f"{class_names[labels[i]]}: {probs[i]:.2f}"
        # label1, color1 = getMaskStateAnalysis(orig_image[startY:endY,startX:endX])
        # label2, color2 = getEmotionsAnalysis2(orig_image[startY:endY,startX:endX])
        # label = label1 + " - " + label2
        cv2.putText(orig_image, label, (startX, startY - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.45, color1, 2)

        cv2.rectangle(orig_image, (startX, startY), (endX, endY), (255, 255, 0), 1)
        # cv2.rectangle(orig_image, (startX + 2, startY + 2), (endX - 2, endY - 2), color1, 1)
        # cv2.rectangle(orig_image, (startX + 4, startY + 4), (endX - 4, endY - 4), color2, 2)
    orig_image = cv2.resize(orig_image, (0, 0), fx=1, fy=1)
    # cv2.imshow('annotated', orig_image)
    print("cost time:{}".format(time.time() - time_time))

def face_detector(image):
    orig = image.copy()
    (h, w) = image.shape[:2]
    blob = cv2.dnn.blobFromImage(image, 1.0, (300, 300),(104.0, 177.0, 123.0))
    print("[INFO] computing face detections...")
    face_model.setInput(blob)
    detections = face_model.forward()
    for i in range(0, detections.shape[2]):
        confidence = detections[0, 0, i, 2]
        if confidence > 0.8:
            box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
            (startX, startY, endX, endY) = box.astype("int")
            (startX, startY) = (max(0, startX - 10), max(0, startY - 10))
            (endX, endY) = (min(w - 1, endX + 10), min(h - 1, endY + 10))
            try:
                face = image[startY:endY, startX:endX]
                emotionsFace = image[startY:endY, startX:endX]
                emotionsFace = cv2.resize(emotionsFace, (48, 48))
                face = cv2.cvtColor(face, cv2.COLOR_BGR2RGB)
                face = cv2.resize(face, (224, 224))
                face = img_to_array(face)
                face = preprocess_input(face)
                face = np.expand_dims(face, axis=0)

                if isMaskDetection:
                    label1, color1 = getMaskStateAnalysis(face)
                    if label1 == "No Mask":
                        label += label1
                        cv2.rectangle(image, (startX, startY), (endX, endY), color1, 2)

                if isEmotionDetection:
                    label2, color2 = getEmotionsAnalysis2(emotionsFace)
                    label += " - " + label2
                    cv2.rectangle(image, (startX + 3, startY + 3), (endX - 3 , endY - 3), color2, 2)

                cv2.putText(image, label, (startX, startY - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.45, color1, 2)
            except Exception as e:
                print("FACE DETECTOR...   ")
                print(e)                  

    cv2.imshow("Output", image)

def getMaskStateAnalysis(face_frame):
    try:
        face = cv2.cvtColor(face_frame, cv2.COLOR_BGR2RGB)
        face = cv2.resize(face, (224, 224))
        face = img_to_array(face)
        face = preprocess_input(face)
        face = np.expand_dims(face, axis=0)
        preds=[]
        preds = mask_model.predict(face)
        for pred in preds:
            (mask, withoutMask) = pred
        maskstate = False
        label = "Mask" if mask > maskConfidence else "No Mask"
        if label == "Mask":
            maskState_sum[0] += 1
            maskstate = True
        else:
            maskState_sum[1] += 1
        color = (0, 255, 0) if label == "Mask" else (0, 0, 255)
        label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)
        return label, color, maskstate

    except Exception as e:
        print("MASK ERROR...  ")
        print(e)
        color = (255, 0, 0)
        return "Error", color

def getEmotionsAnalysis2(img):
    try:
        img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        img = cv2.resize(img, (48, 48))
        img_pixels = img_to_array(img)
        img_pixels = np.expand_dims(img_pixels, axis = 0)
        img_pixels /= 255 #normalize input in [0, 1]

        emotion_predictions = emotion_model.predict(img_pixels)[0,:]
        sum_of_predictions = emotion_predictions.sum()
        for i in range(0, len(emotion_labels)):
            emotion_label = emotion_labels[i]
            emotion_prediction = 100 * emotion_predictions[i] / sum_of_predictions
        
        color = (255, 0, 0)
        
        emotions_percent = 100 * np.max(emotion_predictions) / sum_of_predictions
        emotions_label = emotion_labels[np.argmax(emotion_predictions)]
        emotions_index = np.argmax(emotion_predictions)
        emotions_percent_str = "{:.0f}".format(emotions_percent)

        # if emotions_percent > 95:
        #     if emotions_label is "sad":
        #         emotions_index = 6
        #         emotions_label = "neutral"
        emotions_sum[emotions_index] += 1
        return emotions_label + " %" + emotions_percent_str, color
        # elif emotions_percent > 75:
        #     emotions_sum[3] += 1
        #     return "Happy" + " %" + emotions_percent_str, color
        # else:
        #     emotions_sum[6] += 1
        #     return "Neutral" + " %" + emotions_percent_str, color
    except Exception as e:
        print("EMOTION ERROR...  ")
        print(e)
        emotions_sum[6] += 1
        color = (255, 0, 255)
        return "Error", color

cap = cv2.VideoCapture(input_video)
height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
fps = int(cap.get(cv2.CAP_PROP_FPS))

scale_w = 1/2
scale_h = 1/2

SOLID_BACK_COLOR = (41, 41, 41)
# Setuo video writer
fourcc = cv2.VideoWriter_fourcc(*"XVID")
output_movie = cv2.VideoWriter("Pedestrian_detect.avi", fourcc, 8, (width, height))
if isSocialDistance:
    bird_movie = cv2.VideoWriter("Pedestrian_bird.avi", fourcc, 8, (int(width * scale_w), int(height * scale_h)))
# Initialize necessary variables
frame_num = 0
total_pedestrians_detected = 0
total_six_feet_violations = 0
total_pairs = 0
abs_six_feet_violations = 0
pedestrian_per_sec = 0
sh_index = 1
sc_index = 1
avg_personArray = []

while cap.isOpened():
    time_mainLoop = time.time()
    frame_num += 1
    ret, frame = cap.read()
    frame2 = frame.copy()
    if not ret:
        print("end of the video file...")
        break

    frame_h = frame.shape[0]
    frame_w = frame.shape[1]

    if isSocialDistance is True:
        if frame_num == 1:
            cv2.namedWindow("image")
            cv2.setMouseCallback("image", get_mouse_points)
            num_mouse_points = 0
            first_frame_display = True
            # Ask user to mark parallel points and two points 6 feet apart. Order bl, br, tr, tl, p1, p2
            while True:
                image = frame
                cv2.imshow("image", image)
                cv2.waitKey(1)
                if len(mouse_pts) == 7:
                    cv2.destroyWindow("image")
                    break
                first_frame_display = False
            four_points = mouse_pts

            # Get perspective
            M, Minv = get_camera_perspective(frame, four_points[0:4])
            pts = src = np.float32(np.array([four_points[4:]]))
            warped_pt = cv2.perspectiveTransform(pts, M)[0]
            d_thresh = np.sqrt((warped_pt[0][0] - warped_pt[1][0]) ** 2+ (warped_pt[0][1] - warped_pt[1][1]) ** 2)
            bird_image = np.zeros((int(frame_h * scale_h), int(frame_w * scale_w), 3), np.uint8)

            bird_image[:] = SOLID_BACK_COLOR
            pedestrian_detect = frame
            continue

        #print("Processing frame: ", frame_num)

        # draw polygon of ROI
        # pts = np.array([four_points[0], four_points[1], four_points[3], four_points[2]], np.int32)
        # cv2.polylines(frame, [pts], True, (0, 255, 255), thickness=4)
    
    emotions_sum = [0, 0, 0, 0, 0, 0, 0]
    maskState_sum = [0, 0]
    driverAnalysis_sum = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    coco_sum = [0, 0, 0]
    abs_six_feet_violations = 0
    num_pedestrians = 0
    personCounter = 0    

    if(isEmotionDetection or isMaskDetection):
        num_pedestrians = face_detector3(frame, num_pedestrians)

    # Detect person and bounding boxes using DNN
    if(isPersonDetection or isVehicleDetection):
        time_coco = time.time()
        # person_detector3(frame, num_pedestrians)
        pedestrian_boxes, vehicle_boxes, coco_sum[0], coco_sum[1] = cocoDNN.detect_pedestrians(frame, personConfidence)
        print("coco detect time:{}".format(time.time() - time_coco))
        if(isPersonDetection):
            if len(pedestrian_boxes) > 0:
                # pedestrian_detect = plot_detection_boxes_on_image(frame, pedestrian_boxes, "person")
                if(isSocialDistance):
                    time_social = time.time()
                    warped_pts, bird_image = plot_points_on_bird_eye_view(frame, pedestrian_boxes, M, scale_w, scale_h)
                    six_feet_violations, ten_feet_violations, pairs = plot_lines_between_nodes(warped_pts, bird_image, d_thresh)

                    total_pedestrians_detected += num_pedestrians
                    total_pairs += pairs

                    total_six_feet_violations = six_feet_violations / fps
                    # abs_six_feet_violations = six_feet_violations
                    coco_sum[2] = six_feet_violations
                    pedestrian_per_sec, sh_index = calculate_stay_at_home_index(total_pedestrians_detected, frame_num, fps)
                    print("social dist time:{}".format(time.time() - time_social))
        if(isVehicleDetection):
            if len(vehicle_boxes) > 0:
                vehicle_detect = plot_detection_boxes_on_image(frame, vehicle_boxes, "vehicle")

        print("total coco time:{}".format(time.time() - time_coco))

    #writing result on telemetry
    if isCrowdCounting is True:
        avg_personArray.append(num_pedestrians*4)
        tempNumPedestrian = 0
        if len(avg_personArray) > 12:
            avg_personArray.pop(0)
        
        for i in range(len(avg_personArray)):
            tempNumPedestrian += avg_personArray[i]
        
        num_pedestrians = int(tempNumPedestrian / len(avg_personArray))

    coco_sum[0] = num_pedestrians #num_pedestrians
    telemetryArray = []
    for coco in coco_sum:
        telemetryArray.append(str(coco))
    for emotion in emotions_sum:
        telemetryArray.append(str(emotion))
    for maskstate in maskState_sum:
        telemetryArray.append(str(maskstate))

    print("main loop time:{}".format(time.time() - time_mainLoop))
    telemetryArray.append(str(1 / (time.time() - time_mainLoop)))
    print("telemetry array length %s %s", len(telemetryArray),telemetryArray)
    writeTelemetryFile(telemetryArray)

    # resizedImage = cv2.resize(frame, (640, 360))
    cv2.imshow("Street Cam", frame)
    cv2.waitKey(1)
    output_movie.write(frame)
    if isSocialDistance:
        bird_movie.write(bird_image)

cv2.destroyAllWindows()
writer.release()


#face detector 1 opencv
# face_model = cv2.dnn.readNetFromCaffe(faceConfigFile, faceModelFile)

# face_detector2 onnx
# predictor = onnx.load(onnx_path)
# onnx.checker.check_model(predictor)
# onnx.helper.printable_graph(predictor.graph)
# predictor = backend.prepare(predictor, device="CPU")  # default CPU
# ort_session = ort.InferenceSession(onnx_path)
# input_name = ort_session.get_inputs()[0].name

# emotions_models2["age"] = Age.loadModel()
# emotions_models2["gender"] = Gender.loadModel()
# emotions_models2["race"] = Race.loadModel()

# faceCascade = cv2.CascadeClassifier(workingPath + "/DemoCameraService/HCFF.xml")
# driverClassLabels = ['safe_driving', 'texting_right', 'talking_on_phone_right', 'texting_left', 'talking_on_phone_left',
#                 'operating_radio', 'drinking', 'reaching_behind', 'doing_hair_makeup', 'talking_to_passanger']

# _driverModel = create_model()
# _driverModel.load_weights("DriverWeights.h5")
# _driverModel.compile(loss='categorical_crossentropy',
#               optimizer='rmsprop',
#               metrics=['accuracy'])


# Get video handle


# def getEmotionAnalysis(img):
#     gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#     try:
#         faces = face_cascade.detectMultiScale(gray, 1.3, 5)
#         if len(faces) < 1:
#             emotions_sum[len(emotions_sum) - 1] += 1
#             return

#         for (x, y, w, h) in faces:
#             cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
#             detected_face = img[int(y):int(y+h), int(x):int(x+w)]
#             detected_face = cv2.cvtColor(detected_face, cv2.COLOR_BGR2GRAY)  # transform to gray scale
#             detected_face = cv2.resize(detected_face, (48, 48))  # resize to 48x48

#             img_pixels = img_to_array(detected_face)
#             img_pixels = np.expand_dims(img_pixels, axis=0)
#             img_pixels /= 255
#             emotion_predictions = emotions_model.predict(img_pixels)
#             max_index = np.argmax(emotion_predictions[0])
#             emotion = emotions[max_index]
#             emotions_sum[max_index] += 1
#             cv2.putText(img, emotion, (int(x), int(y)),cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2)
#             cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
#             cv2.imshow("Emotions", img)

#     except Exception as e:
#         print("emotion detection error")
#         print(e)
#         emotions_sum[len(emotions_sum) - 1] += 1

#     # print(emotions_sum)
#     #cv2.imshow('img', img)

# def getDriverAnalysis(frame, driverAnalysis_sum):
    # try:
    #     resImg = cv2.resize(frame, (150, 150))
    #     image_arr = img_to_array(resImg)
    #     image_arr = np.expand_dims(image_arr, axis=0)
    #     image_arr /= 255

    #     # classify given an image
    #     predictions = _driverModel.predict(image_arr)

    #     # get human-readable labels of the preditions, as well as the corresponding probability
    #     decoded_predictions = dict(zip(driverClassLabels, predictions[0]))

    #     # sort dictionary by value
    #     decoded_predictions = sorted(
    #         decoded_predictions.items(), key=operator.itemgetter(1), reverse=True)

    #     print("predictions %s", decoded_predictions)
    #     count = 1
    #     for key, value in decoded_predictions[:5]:
    #         print("{}. {}: {:8f}%".format(count, key, value*100))
    #         count += 1
    #         # print image
    #     cv2.imshow(resImg)
    #     return driverAnalysis_sum
    # except Exception as e:
    #     print("prediction failed")
    #     print(e)
    #     return driverAnalysis_sum