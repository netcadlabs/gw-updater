# Already trained model available @
# https://github.com/tensorflow/models/tree/master/research/object_detection
# was used as a part of this code.

import os
import tensorflow as tf
from vision.utils import label_map_util
os.environ["CUDA_VISIBLE_DEVICES"] = "-1"
import cv2
import numpy as np

class ObjectDetectionModel:
    def __init__(self, modelCkpt, labelsPath):
        # modelCkpt = modelPath + "frozen_inference_graph.pb"
        # labelsPath += "mscoco_label_map.pbtxt"
        num_classes = 90
        detection_graph = tf.Graph()
        with detection_graph.as_default():
            od_graph_def = tf.compat.v1.GraphDef() 
            with tf.io.gfile.GFile(modelCkpt, "rb") as fid:
                serialized_graph = fid.read()
                od_graph_def.ParseFromString(serialized_graph)
                tf.import_graph_def(od_graph_def, name="")

        label_map = label_map_util.load_labelmap(labelsPath)
        categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=num_classes, use_display_name=True)
        self.category_index = label_map_util.create_category_index(categories)
        
        config = tf.compat.v1.ConfigProto()
        config.gpu_options.allow_growth = True
        self.sess = tf.compat.v1.InteractiveSession(graph=detection_graph, config=config)
        self.image_tensor = detection_graph.get_tensor_by_name("image_tensor:0")
        self.detection_boxes = detection_graph.get_tensor_by_name("detection_boxes:0")
        self.detection_scores = detection_graph.get_tensor_by_name("detection_scores:0")
        self.detection_classes = detection_graph.get_tensor_by_name("detection_classes:0")
        self.num_detections = detection_graph.get_tensor_by_name("num_detections:0")

    def get_category_index(self):
        return self.category_index

    def detect_pedestrians(self, frame, confidence):
        # Actual detection.
        # input_frame = cv2.resize(frame, (350, 200))
        input_frame = frame

        # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
        image_np_expanded = np.expand_dims(input_frame, axis=0)
        (boxes, scores, classes, num) = self.sess.run(
            [
                self.detection_boxes,
                self.detection_scores,
                self.detection_classes,
                self.num_detections,
            ],
            feed_dict={self.image_tensor: image_np_expanded},
        )

        classes = np.squeeze(classes).astype(np.int32)
        boxes = np.squeeze(boxes)
        scores = np.squeeze(scores)
        detection_score_threshold = confidence
        pedestrian_boxes = []
        vehicle_boxes = []
        total_pedestrians = 0
        total_vehicle = 0
        for i in range(int(num[0])):
            if classes[i] in self.category_index.keys():
                class_name = self.category_index[classes[i]]["name"]
                # print(class_name)
                if class_name == "person" and scores[i] > detection_score_threshold:
                    (frame_height, frame_width) = input_frame.shape[:2]
                    image = input_frame.copy()
                    total_pedestrians += 1
                    score_pedestrian = scores[i]
                    pedestrian_boxes.append(boxes[i])
                    #print(np.squeeze(boxes)[i])

                if class_name == "car" and scores[i] > detection_score_threshold:
                    (frame_height, frame_width) = input_frame.shape[:2]
                    image = input_frame.copy()
                    total_vehicle += 1
                    score_pedestrian = scores[i]
                    pedestrian_boxes.append(boxes[i])

        return pedestrian_boxes, vehicle_boxes, total_pedestrians, total_vehicle



                    #print(np.squeeze(boxes)[i])
                    
                    # ymin = int(boxes[i][0] * 0.9 * frame_height)
                    # xmin = int(boxes[i][1] * 0.9 * frame_width)
                    # ymax = int(boxes[i][2] * 1.1 * frame_height)
                    # xmax = int(boxes[i][3] * 1.1 * frame_width)
                    # # print(frame_height, frame_width)
                    # # print("xmin %s ymin %s xmax %s ymax %s", str(xmin), str(ymin), str(xmax), str(ymax))
                    # crop_img = image[ymin:ymax,xmin:xmax]
                    # imageList.append(image[ymin:ymax,xmin:xmax])