"""
Serial olarak gateway'e bağlanmış olan arduino ile bağlantı kurmaya
yarayan örnek connector.

connector ayar dosyası : etc/thingsboard-gateway/config/custom_serial_ardunio_kablo.json
connector'un kullandigi converter (ayar dosyasında belirtilen) :  CustomArduinoSerialConverter (custom_arduino_serial_converter.py)
"""

import time
from threading import Thread
from random import choice
from string import ascii_lowercase
import datetime
import serial
from gpiozero import LED
from gpiozero import CPUTemperature, LoadAverage, DiskUsage
from datetime import datetime, date
from datetime import timezone, timedelta
import requests
import json
import re
import subprocess
import psutil
import sys


workingPath = '/var/lib/thingsboard_gateway/extensions/gateway/'
sys.path.append(workingPath)
from LuminetRuleAdapter import LuminetRuleAdapter

# Import base class for connector and logger
from thingsboard_gateway.connectors.connector import Connector, log
from thingsboard_gateway.tb_utility.tb_utility import TBUtility

class GatewayConnector(Thread, Connector):
    def __init__(self, gateway, config, connector_type):
        super().__init__()    # Initialize parents classes
        self.statistics = {'MessagesReceived': 0,
                           'MessagesSent': 0}    # Dictionary, will save information about count received and sent messages.
        # Save configuration from the configuration file.
        self.__config = config
        log.debug("NDU - config %s", config)
        # Save gateway object, we will use some gateway methods for adding devices and saving data from them.
        self.__gateway = gateway
        self.setName(self.__config.get("name", "Custom %s connector " % self.get_name() + ''.join(choice(ascii_lowercase) for _ in range(5))))    # get from the configuration or create name for logs.
        # Send message to logger
        log.info("Starting Custom %s connector", self.get_name())
        self.daemon = True    # Set self thread as daemon
        self.stopped = True    # Service variable for check state
        self.__connected = False    # Service variable for check connection to device
        # Dictionary with devices, will contain devices configurations, converters for devices and serial port objects
        self.__devices = {}
        self.__masterDeviceName = self.__config.get("devices")[0].get("name")
        self.__telemetry_config = self.__config.get("telemetry")
        self.__attr_config = self.__config.get("attributes")
        self.__attr_updates = self.__config.get("attributeUpdates")
        self.__capabilities = self.__config.get("capabilities")
        # Call function to load converters and save it into devices dictionary
        self.__load_converters(connector_type)
        self.lastConnectionCheck = 0
        # first interval(seconds) to reconnect device
        self.reconnect_interval = 5
        self.reconnect_count = 0  # number of reconnection attempt
        self.__connect_to_devices()    # Call function for connect to devices
        self.__waitSession = False
        self.__antennaConfigFinish = False
        self.deviceLastAttributes = {}
        self.__setAttributeQuery = {}
        for device in config.get("devices"):
            self.deviceLastAttributes[device.get("name")] = {}
            self.__setAttributeQuery[device.get("name")] = [""]

        self.__ruleInstance = LuminetRuleAdapter.Instance()

        # log.info("Rule Controller utc offset %s ", str(self.__utcOffset))
        # log.info("Rule Controller equation elements %s ", self.__ruleEquationElementList)
        #self.__ruleController = 
        log.info('Custom connector %s initialization success.',
                 self.get_name())    # Message to logger
        log.info("Devices in configuration file found: %s ", '\n'.join(
            device for device in self.__devices))    # Message to logger

    # Function for opening connection and connecting to devices
    def __connect_to_devices(self):
        device_config = self.__devices[self.__masterDeviceName]['device_config']
        log.debug("NDU - gateway connection established for %s",device_config["name"])
        self.__gateway.add_device(device_config["name"], {"connector": self}, device_type=device_config["type"])
        self.__connected = True            
        self.lastConnectionCheck = time.time()

    def open(self):    # Function called by gateway on start
        self.stopped = False
        self.start()

    def get_name(self):    # Function used for logging, sending data and statistic
        return self.name

    def is_connected(self):    # Function for checking connection state
        return self.__connected

    # Function for search a converter and save it.
    def __load_converters(self, connector_type):
        devices_config = self.__config.get('devices')
        try:
            if devices_config is not None:
                for device_config in devices_config:
                    if device_config.get('converter') is not None:
                        converter = TBUtility.check_and_import(
                            connector_type, device_config['converter'])
                        self.__devices[device_config['name']] = {'converter': converter(device_config),
                                                                 'device_config': device_config}
                        log.debug("NDU - __load_converters converter %s loaded with : %s",
                                  device_config['converter'], device_config)
                    else:
                        log.error(
                            'Converter configuration for the custom connector %s -- not found, please check your configuration file.', self.get_name())
            else:
                log.error(
                    'Section "devices" in the configuration not found. A custom connector %s has being stopped.', self.get_name())
                self.close()
        except Exception as e:
            log.exception(e)

    def getGatewayTelemetry(self, currentDevice):
        try:
            #log.debug("NDU Gateway Connector loop")
            # personcount = self.GetFrameResult()
            currentConfig = self.__devices[currentDevice]['device_config']
            #log.debug('NDU - found person %s', currentConfig)
            
            result_dict = {
                'deviceName': currentConfig.get('name', 'CustomSerialDevice'),
                'deviceType': currentConfig.get('deviceType', 'default'),
                'attributes': [],
                'telemetry': [],
                'deviceName': currentConfig['name']
                }

            response_time = 0
            cpuClockRate = 0
            ambientTemp = 0
            ramUsage = 0
            try:
                output = subprocess.check_output(['ping', '-c', '4', '-q', 'smartapp.netcad.com'])
                output = output.decode('utf8')
                statistic = re.search(r'(\d+\.\d+/){3}\d+\.\d+', output).group(0)
                avg_time = re.findall(r'\d+\.\d+', statistic)[1]
                response_time = float("{:.2f}".format(float(avg_time)))

            except subprocess.CalledProcessError:
                response_time = 99999999
            
            try:            
                f = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq")
                cpuClockRate = float("{:.2f}".format(int(f.read()) / 1024))
                tFile = open('/sys/class/thermal/thermal_zone0/temp')
                ambientTemp = float("{:.2f}".format(float(tFile.read()) / 1000))
                ramUsage = float("{:.2f}".format(psutil.virtual_memory()[2]))
            except Exception as e:
                log.debug("Gateway Telemetry Failed")
                self.log_exception(e)

            #log.info( "NDU - load average : %s", cpuClockRate)
            #log.info( "NDU - ping server : %s", response_time)
            #log.info( "NDU - disk usage : %s", DiskUsage().usage)
            #log.info( "NDU - memory usage : %s", ramUsage)
            #log.info( "NDU - ambient temp : %s", ambientTemp)

            result_dict['telemetry'].append({"CPU TEMP": CPUTemperature().temperature})
            result_dict['telemetry'].append({"CPU CLOCK": cpuClockRate})
            result_dict['telemetry'].append({"GPU CLOCK": 0})
            result_dict['telemetry'].append({"NETWORK LATENCY": response_time})
            result_dict['telemetry'].append({"MEMORY USAGE": ramUsage})
            result_dict['telemetry'].append({"DISK USAGE": float("{:.2f}".format(DiskUsage().usage))})
            result_dict['telemetry'].append({"AMBIENT TEMP": ambientTemp})
            result_dict['telemetry'].append({"LATITUDE": float("{:.2f}".format(self.__ruleInstance.GWLatitude()))})
            result_dict['telemetry'].append({"LONGITUDE": float("{:.2f}".format(self.__ruleInstance.GWLongitude()))})
            result_dict['telemetry'].append({"SOLAR NOON": str(self.__ruleInstance.SolarNoon())})
            result_dict['telemetry'].append({"SUNSET": str(self.__ruleInstance.Sunset())})
            result_dict['telemetry'].append({"SUNRISE": str(self.__ruleInstance.Sunrise())})
            self.__gateway.send_to_storage(self.get_name(), result_dict)
            #log.info( "NDU - converted Data send server : %s", result_dict)

            
        except Exception as e:
            log.debug("NDU Gateway Connector Failed")
            self.log_exception(e)

    def run(self):    # Main loop of thread
        #self.setAntennaConfig()
        #self.setRuleController()
        telemetryCounter = 0
        try:
            while True:
                # @warn read serial connections
                telemetryCounter += 1
                #log.debug("NDU - TELEMETRY COUNTER %s", str(telemetryCounter))
                if(telemetryCounter > 60):
                    telemetryCounter = 0
                    self.getGatewayTelemetry(self.__masterDeviceName)
                    time.sleep(1)
                time.sleep(1)
            
        except Exception as e:
            self.log_exception(e)
    
    def getRuleController(self):
        return self.__ruleController

    def log_exception(self, e):
        if hasattr(e, 'message'):
            log.error(e.message)
        else:
            log.exception(e)

    def close(self):    # Close connect function, usually used if exception handled in gateway main loop or in connector main loop
        self.stopped = True
        self.__gateway.del_device(self.__devices[self.__masterDeviceName])
        if self.__devices[self.__masterDeviceName]['serial'].isOpen():
            self.__devices[self.__masterDeviceName]['serial'].close()

    # Function used for processing attribute update requests from ThingsBoard
    def on_attributes_update(self, content):
        device_name = content["device"]
        #log.debug("NDU - on_attributes_update device : %s , content : %s", content, device_name)
        if self.__devices.get(device_name) is not None:
            # log.debug("NDU - device : %s", self.__devices.get(device_name))
            device_config = self.__devices[device_name].get("device_config")
            if device_config is not None:
                # log.debug("NDU - device_config %s", device_config)
                if device_config.get("attributeUpdates") is not None:
                    requests = device_config["attributeUpdates"]
                    for request in requests:
                        attribute = request.get("name")
                        # sadece config icerisinde olan attribute isimleri cihaza gonderiliyor
                        if attribute is not None and attribute in content["data"]:
                            # log.debug("NDU - gelen attribute : %s", attribute)
                            try:
                                value = content["data"][attribute]
                                self.deviceLastAttributes[device_name][attribute] = value
                                # @warn saat eklenecek
                                # @warn string kontrol edilecek
                                time.sleep(.01)
                            except Exception as e:
                                self.log_exception(e)

    def server_side_rpc_handler(self, content):
        # log.info("RPC KOMUT GELDİ %s",content)
        if content is not None and content["device"] is not None:
            if content["data"] is None or content["data"].get("method") is None:
                log.debug(
                    "NDU - rpc data or method not found in content : %s", content)
                pass

            device_name = content["device"]
            data = content["data"]
            method = content["data"].get("method")
            # log.debug("NDU - rpc method : %s", method)
            # log.debug("NDU - rpc data : %s", data)

            if self.__devices.get(device_name) is not None:
                device_config = self.__devices[device_name].get(
                    "device_config")
                if device_config is not None and device_config.get("capabilities") is not None:
                    capabilities = device_config.get("capabilities")
                    found = False
                    for capability in capabilities:
                        if method == capability["method"]:
                            self.run_capability(device_name, capability, data)
                            found = True
                    if not found:
                        log.debug("NDU - capability not found : %s", method)
                else:
                    log.debug("NDU - device capabilities not found.")

    # data > {'id': 28, 'method': 'setLedStatus', 'params': {'status': 0}}
    # capability > {'method': 'setLedStatus', 'description': 'Set led status', 'stringToDevice': 'L1=${status}\n', 'singleParams': False, 'parameters': [{'name': 'status', 'type': 'number'}]}
    def run_capability(self, device_name, capability, data):
        try:
            stringToDevice = capability["stringToDevice"]  # check if exist

            log.debug("NDU - capability will run : %s", capability)
            if capability.get("parameters", None) is None:
                if capability.get("singleParams", False):
                    # log.debug("NDU - running singleparams capability")
                    stringToDevice = stringToDevice.replace(
                        "${" + "params" + "}", str(data["params"]))
                # elif capability.get("singleParams", True): # do nothing
                #     log.debug("NDU - running no paramter capability")
            else:
                for capability_param in capability["parameters"]:
                    for param in data["params"]:
                        # log.debug("NDU - gelen param : %s, capability_param.name : %s", param, capability_param["name"])
                        # log.debug("NDU - gelen type(param) : %s, type(capability_param.name) : %s", type(param), type(capability_param["name"]))
                        if str(capability_param["name"]) == str(param):
                            replace_expression = "${" + \
                                capability_param["name"] + "}"
                            log.debug("NDU - replace_expression %s with %s",
                                      replace_expression, str(data["params"].get(param)))
                            stringToDevice = stringToDevice.replace(
                                replace_expression, str(data["params"].get(param)))

            # log.debug("NDU - stringToDevice : %s", stringToDevice)
            # checksum eklenecek
            str_to_send = str(stringToDevice).encode("UTF-8")
            self.__devices[device_name]["serial"].write(str_to_send)

        except Exception as e:
            self.log_exception(e)
