#     Copyright 2020. ThingsBoard
#
#     Licensed under the Apache License, Version 2.0 (the "License");
#     you may not use this file except in compliance with the License.
#     You may obtain a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#     Unless required by applicable law or agreed to in writing, software
#     distributed under the License is distributed on an "AS IS" BASIS,
#     WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#     See the License for the specific language governing permissions and
#     limitations under the License.

from thingsboard_gateway.connectors.converter import Converter, log

import random
import psutil

class GatewayConverter(Converter):
    def __init__(self, config):
        self.__config = config
        self.result_dict = {
            'deviceName': config.get('name', 'CustomSerialDevice'),
            'deviceType': config.get('deviceType', 'default'),
            'attributes': [],
            'telemetry': []
        }

    def convert(self, config, data: bytes):
        keys = ['attributes', 'telemetry']
        for key in keys:
            self.result_dict[key] = []
            if self.__config.get(key) is not None:
                for config_object in self.__config.get(key):

                    value = round(random.uniform(20, 40), 0)

                    if str(config_object['key']) == str("CPU"):
                        value = psutil.cpu_percent() * 2

                    if str(config_object['key']) == str("MEMORY"):
                        value = psutil.virtual_memory()[2] * 2

                    if str(config_object['key']) == str("temp"):
                        value = round(random.uniform(40, 50), 3)

                    if str(config_object['key']) == str("humidity"):
                        value = round(random.uniform(10, 20), 4)
                    
                    converted_data = {config_object['key']: value}

                    self.result_dict[key].append(converted_data)

        log.debug("Converted data: %s", self.result_dict)

        self.result_dict['telemetry'].append(converted_data)
        return self.result_dict

