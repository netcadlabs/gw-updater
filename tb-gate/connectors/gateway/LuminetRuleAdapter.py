from threading import Thread
from random import choice
from string import ascii_lowercase
import datetime
import serial
from gpiozero import LED
from gpiozero import CPUTemperature, LoadAverage, DiskUsage
from datetime import datetime, date
from datetime import timezone, timedelta
import requests
import json
import re
import subprocess
import psutil
import time

# Import base class for connector and logger
from thingsboard_gateway.connectors.connector import Connector, log
from thingsboard_gateway.tb_utility.tb_utility import TBUtility

    
class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Other than that, there are
    no restrictions that apply to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    Limitations: The decorated class cannot be inherited from.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def Instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)

@Singleton
class LuminetRuleAdapter(object):
    def __init__(self):        

        self.antennaCommand = ["AT", "AT+C070", "AT+FU3", "AT+B9600", "AT+P8", "AT+RX", "AT+V"]
        
        self.__gatewayLatitude = 40.9908628
        self.__gatewayLongitude = 29.0728532

        self.__sendingDIMValue = 0
        self.__sunset = ""
        self.__solarNoon = ""
        self.__sunrise = ""

        self.__defaultSunrise = ""
        self.__defaultSunset = ""
        self.__currentDimPower = 0

        self.__twilightTimeList = []
        self.__ruleEquationElementList = [
            3.7191E-13, -3.0534E-10, 9.9427E-8, -1.65907E-5, 1.50725E-3, -7.11849E-2, 1.37675, 0, 20]

        self.__utcOffset = (datetime.fromtimestamp(
            time.time()) - datetime.utcfromtimestamp(time.time())).total_seconds()/3600
        self.__timeOffset = 30  # minute

        self.twilightTimeCalculation()

        log.info("Rule Controller utc offset %s ", str(self.__utcOffset))
        log.info("Rule Controller equation elements %s ", self.__ruleEquationElementList)
    
    def setAntennaConfig(self):
        try:
            comm = self.__devices[self.__masterDeviceName]["serial"]
            log.debug("NDU antenna config set pin low")
            antennaSetPin = LED(17)
            antennaSetPin.off()
            time.sleep(2)
            self.serialCommunicator("AT+DEFAULT\r\n", comm, 2)
            time.sleep(2)
            self.serialCommunicator("AT+C070\r\n", comm, 2)
            time.sleep(2)
            self.serialCommunicator("AT+RX\r\n", comm, 7)
            time.sleep(2)
            while self.__antennaConfigFinish == False:
                log.debug("NDU wait antenna config ")
                time.sleep(1)

            log.debug("NDU antenna config set pin high")
            antennaSetPin.on()    
            time.sleep(1)
        except Exception as e:
            self.log_exception(e)
    
    # RULE CONTROLLER METHOD
    def getLightningRuleCalculation(self):
        currentTime = datetime.now()
        log.info("cRule Controller current time %s ", str(currentTime))
        if currentTime < self.__sunset or currentTime > self.__sunrise:
            log.info("Rule Controller gündüz")
            return 0
        else:

            d1_ts = time.mktime(currentTime.timetuple())
            d2_ts = time.mktime(self.__sunset .timetuple())
            timeValue = (d1_ts - d2_ts) / (3600)

            log.info("Rule Controller time diff %s ", str(timeValue))

            listLength = len(self.__ruleEquationElementList)
            totalValue = 0
            for element in self.__ruleEquationElementList:
                listLength -= 1
                totalValue += (element * pow(timeValue, (listLength)))
                # print(str(totalValue) + " " + str(listLength))

            if totalValue > 100:
                totalValue = 100
            log.info("Rule Controller utc offset %s %s ara toplam", str(totalValue), str(timeValue))
            return int(totalValue * 6.5)

    def getLocationFromServer(self):
        return

    def twilightTimeCalculation(self):
        format = '%Y-%m-%d %I:%M:%S %p'

        currentTime = datetime.now()

        dayStr = str(date.today()) + " "
        results = self.getTwilightTimeFromApi(str(date.today()))
        self.__sunset = datetime.strptime(
            dayStr + str(results.get("sunset")), format) + timedelta(hours=self.__utcOffset)
        self.__solarNoon = datetime.strptime(
            dayStr + str(results.get("solar_noon")), format) + timedelta(hours=self.__utcOffset)
        self.__sunrise = datetime.strptime(
            dayStr + str(results.get("sunrise")), format) + timedelta(hours=self.__utcOffset)

        timeDiff = (self.__solarNoon - currentTime).days
        log.info("Rule Controller time diff twilight %s ", str(timeDiff))

        if timeDiff == 0:
            checkDay = date.today() + timedelta(hours=-24)
            results = self.getTwilightTimeFromApi(str(checkDay))
            dayStr = str(checkDay) + " "
            self.__sunset = datetime.strptime(
                dayStr + str(results.get("sunset")), format) + timedelta(hours=self.__utcOffset)
            self.__solarNoon = datetime.strptime(
                dayStr + str(results.get("solar_noon")), format) + timedelta(hours=self.__utcOffset)
        else:
            checkDay = date.today() + timedelta(hours=24)
            results = self.getTwilightTimeFromApi(str(checkDay))
            dayStr = str(checkDay) + " "
            self.__sunrise = datetime.strptime(
                dayStr + str(results.get("sunrise")), format) + timedelta(hours=self.__utcOffset)

        log.info("Rule Controller sunrise  %s ", str(self.__sunrise))
        log.info("Rule Controller sunset  %s ",str(self.__sunset))
        log.info("Rule Controller solar noon  %s ", str(self.__solarNoon))
        return

    def getTwilightTimeFromApi(self, requestDate):
        # https://api.sunrise-sunset.org/json?lat=41.0687831&lng=29.0110758&date=today
        requestString = "https://api.sunrise-sunset.org/json?lat="
        requestString += str(self.__gatewayLatitude) + "&lng="
        requestString += str(self.__gatewayLongitude) + "&date=" + requestDate

        log.info("Rule Controller request string  %s ", requestString)
        r = requests.get(requestString).json()
        if "OK" in str(r.get("status")):
            log.info("Rule Controller ok")
        else:
            self.getTwilightTimeFromApi(requestDate)
            return

        results = r.get("results")
        log.info("Rule Controller twilight result %s ", results)
        return results

    def getTwilightTimeList(self):
        return self.__twilightTimeList

    def checkLuminetLightValue(self, isRule, isMove, isAmbientLight, dimPower):
        self.__sendingDIMValue = 0
        if isRule:
            self.__sendingDIMValue = self.getLightningRuleCalculation()
 
        # elif isAmbientLight:
        #     if self.__sendingDIMValue == 0:
        #         self.__sendingDIMValue = 150
        #     self.__sendingDIMValue *= 1.3
        else:
            self.__sendingDIMValue = int(dimPower)

        #using for return ordinary state
        self.__currentDimPower = self.__sendingDIMValue
            
        if isMove:
            self.__sendingDIMValue = int(self.__sendingDIMValue * 1.3) 
        
        if self.__sendingDIMValue > 650:  # max dim value
            self.__sendingDIMValue = 650

        return self.__sendingDIMValue

    def setSharedAttributeFromServer(self, attributes):
        return

    def GWLatitude(self):
        return self.__gatewayLatitude
    
    def GWLongitude(self):
        return self.__gatewayLongitude
    
    def SolarNoon(self):
        return self.__solarNoon
    
    def Sunset(self):
        return self.__sunset
    
    def Sunrise(self):
        return self.__sunrise

    def CurrentDimPower(self):
        return self.__currentDimPower