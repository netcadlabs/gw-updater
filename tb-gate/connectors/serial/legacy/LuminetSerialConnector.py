"""
Serial olarak gateway'e bağlanmış olan arduino ile bağlantı kurmaya
yarayan örnek connector.

connector ayar dosyası : etc/thingsboard-gateway/config/custom_serial_ardunio_kablo.json
connector'un kullandigi converter (ayar dosyasında belirtilen) :  CustomArduinoSerialConverter (custom_arduino_serial_converter.py)
"""

import time
from threading import Thread
from random import choice
from string import ascii_lowercase
import datetime
import serial
from gpiozero import LED
import subprocess

# Import base class for connector and logger
from thingsboard_gateway.connectors.connector import Connector, log
from thingsboard_gateway.tb_utility.tb_utility import TBUtility

import sys
workingPath = '/var/lib/thingsboard_gateway/extensions/gateway/'
sys.path.append(workingPath)
from LuminetRuleAdapter import LuminetRuleAdapter

class LuminetSerialConnector(Thread, Connector):
    def __init__(self, gateway, config, connector_type):
        super().__init__()    # Initialize parents classes
        self.statistics = {'MessagesReceived': 0,
                           'MessagesSent': 0}    # Dictionary, will save information about count received and sent messages.
        # Save configuration from the configuration file.
        self.__config = config
        log.debug("NDU - config %s", config)
        # Save gateway object, we will use some gateway methods for adding devices and saving data from them.
        self.__gateway = gateway
        self.setName(self.__config.get("name", "Custom %s connector " % self.get_name() + ''.join(choice(ascii_lowercase) for _ in range(5))))    # get from the configuration or create name for logs.
        # Send message to logger
        log.info("Starting Custom %s connector", self.get_name())
        self.daemon = True    # Set self thread as daemon
        self.stopped = True    # Service variable for check state
        self.__connected = False    # Service variable for check connection to device
        # Dictionary with devices, will contain devices configurations, converters for devices and serial port objects
        self.__devices = {}
        self.__masterDeviceName = self.__config.get("devices")[0].get("name")
        # Call function to load converters and save it into devices dictionary
        self.__load_converters(connector_type)
        self.lastConnectionCheck = 0
        # first interval(seconds) to reconnect device
        self.reconnect_interval = 5
        self.reconnect_count = 0  # number of reconnection attempt
        self.__connect_to_devices()    # Call function for connect to devices
        self.__waitSession = False
        self.__antennaConfigFinish = False
        self.deviceLastAttributes = {}
        self.__setAttributeQuery = []
        
        self.__ruleInstance = LuminetRuleAdapter.Instance()
        self.__setDimPower = 0
        self.__timerControl = False
        self.__isRuleControlEnable = False
        self.__isLightningOn = False
        self.__defaultFiproValue = [180,120,780,500,300]

        for device in config.get("devices"):
            self.deviceLastAttributes[device.get("name")] = {}
            #self.__setAttributeQuery[device.get("name")] = [""]
        
        self.antennaCommand = ["AT", "AT+C070", "AT+FU3", "AT+B9600", "AT+P8", "AT+RX", "AT+V"]
        #self.checkCommandResponse = True
        log.info('Custom connector %s initialization success.',
                 self.get_name())    # Message to logger
        log.info("Devices in configuration file found: %s ", '\n'.join(
            device for device in self.__devices))    # Message to logger

    # Function for opening connection and connecting to devices
    def __connect_to_devices(self):
        device_config = self.__devices[self.__masterDeviceName]['device_config']
        try:    # Start error handler
            connection_start = time.time()

            # log.debug("NDU - for device_config %s", device_config)
            if self.__devices[self.__masterDeviceName].get("serial") is None \
                    or self.__devices[self.__masterDeviceName]["serial"] is None \
                    or not self.__devices[self.__masterDeviceName]["serial"].isOpen():    # Connect only if serial not available earlier or it is closed.
                self.__devices[self.__masterDeviceName]["serial"] = None

                while self.__devices[self.__masterDeviceName]["serial"] is None or not self.__devices[self.__masterDeviceName]["serial"].isOpen():    # Try connect
                    # connection to serial port with parameters from configuration file or default
                    # log.debug("NDU - for device %s", device_config)
                    self.reconnect_count = self.reconnect_count + 1
                    log.debug("NDU - (%s) trying to serial connection for %s",
                              self.reconnect_count, device_config["name"])

                    self.__devices[self.__masterDeviceName]["serial"] = serial.Serial(port=device_config.get('port', '/dev/ttyAMA4'),
                                                                                      baudrate=device_config.get(
                                                                                          'baudrate', 115200),
                                                                                      bytesize=device_config.get(
                                                                                          'bytesize', serial.EIGHTBITS),
                                                                                      parity=device_config.get(
                                                                                          'parity', serial.PARITY_NONE),
                                                                                      stopbits=device_config.get(
                                                                                          'stopbits', serial.STOPBITS_ONE),
                                                                                      timeout=device_config.get(
                                                                                          'timeout', 1),
                                                                                      xonxoff=device_config.get(
                                                                                          'xonxoff', False),
                                                                                      rtscts=device_config.get(
                                                                                          'rtscts', False),
                                                                                      write_timeout=device_config.get(
                                                                                          'write_timeout', None),
                                                                                      dsrdtr=device_config.get(
                                                                                          'dsrdtr', False),
                                                                                      inter_byte_timeout=device_config.get(
                                                                                          'inter_byte_timeout', None),
                                                                                      exclusive=device_config.get('exclusive', None))
                    time.sleep(.1)
                    
                    if time.time() - connection_start > 100:    # Break connection try if it setting up for 10 seconds
                        log.error(
                            "Connection refused per timeout for device %s", device_config.get("name"))
                        break
            else:
                log.debug( "NDU - already connected and serial port is open for %s", device_config.get("name"))

        except serial.serialutil.SerialException as se:
            log.error("Port %s for device %s - not found", device_config.get('port', '/dev/ttyAMA4'), self.__devices[self.__masterDeviceName])
            log.exception(se)
        except Exception as e:
            log.exception(e)
        else:    # if no exception handled - add device and change connection state
            log.debug("NDU - serial connection established for %s",device_config["name"])
            self.__gateway.add_device(device_config["name"], {"connector": self}, device_type=device_config["type"])
            self.__connected = True
            self.reconnect_count = 0
            for device in self.__devices:
                device_config = self.__devices[device]['device_config']
                deviceType = device_config.get("type")
                log.debug("NDU devices for  : %s type  %s", str(device), str(deviceType))
                if str(deviceType) is not str("Master"):
                    log.debug("NDU devices : %s", str(device))
                    self.__gateway.add_device(device_config["name"], {"connector": self}, device_type=device_config["type"])
            

        self.lastConnectionCheck = time.time()

    def open(self):    # Function called by gateway on start
        self.stopped = False
        self.start()

    def get_name(self):    # Function used for logging, sending data and statistic
        return self.name

    def is_connected(self):    # Function for checking connection state
        return self.__connected

    # Function for search a converter and save it.
    def __load_converters(self, connector_type):
        devices_config = self.__config.get('devices')
        try:
            if devices_config is not None:
                for device_config in devices_config:
                    if device_config.get('converter') is not None:
                        converter = TBUtility.check_and_import(
                            connector_type, device_config['converter'])
                        self.__devices[str(device_config['name']).strip()] = {'converter': converter(device_config),
                                                                 'device_config': device_config}
                        #log.debug("NDU - XXXX %s found added",device_config['name'])
                        log.debug("NDU - __load_converters converter %s loaded with : %s",
                                  device_config['converter'], device_config)
                    else:
                        log.error(
                            'Converter configuration for the custom connector %s -- not found, please check your configuration file.', self.get_name())
            else:
                log.error(
                    'Section "devices" in the configuration not found. A custom connector %s has being stopped.', self.get_name())
                self.close()
        except Exception as e:
            log.exception(e)
    
    def sendData2Server(self, deviceIndex, currentDevice):
        comm = self.__devices[self.__masterDeviceName]["serial"]
        try:
            if self.__receivedString[deviceIndex] == currentDevice:
                #log.info( "NDU - received string : %s", self.__receivedString[deviceIndex])
                #log.info( "NDU - current device : %s", currentDevice)
                #for key, value in self.__devices.items() :
                #    log.debug("device : %s", key)
                device =  self.__devices.get(str(currentDevice).strip(), None)
                if device is None:
                    log.debug("NDU - %s not found __devices", currentDevice)
                    return

                log.info( "NDU -  devicee : %s", device)
                currentConfig = device['device_config']
                currentConverter = self.__devices[currentDevice]['converter']
                converted_data = currentConverter.convert(currentConfig, self.__dataFromDevice)
                if len(converted_data["attributes"]) > 0 or len(converted_data["telemetry"]) > 0:
                    self.__gateway.send_to_storage(self.get_name(), converted_data)
                    comm.write(str("#SNO\r\n").encode('UTF-8'))
                    log.info( "NDU - converted Data send server : %s", converted_data)
                else:
                    log.info( "NDU - converted Data NULL : %s", converted_data)
                

            else:
                log.debug("NDU - converted Data short")
        except Exception as e:
            self.log_exception(e)

    def sendData2Device(self, sendCommand):
        comm = self.__devices[self.__masterDeviceName]["serial"]
        log.info("NDU - send command : %s", sendCommand)
        self.serialCommunicator(str(sendCommand), comm)
        
    def onlyListenDevice(self, comm, readAgain = 1):
        received_character = b''
        data_from_device = b''
        readCounter = 0
        while received_character != b'\n':  # We will read until receive LF symbol
            try:
                if comm is None or not hasattr(comm, 'read'):
                    # log.debug("NDU - port is not ready..")
                    self.__connected = False
                    break
                try:
                    received_character = comm.read(1)    # Read one symbol per time
                    if received_character.decode("utf-8") == '':
                        readCounter += 1
                    if readCounter > 2:
                        break
                except AttributeError as e:
                    log.debug("NDU - AttributeError")
                    if comm is None:
                        self.__devices[self.__masterDeviceName]["serial"] = None
                        self.__connected = False
                        raise e
                except serial.serialutil.SerialException as se:
                    log.debug("NDU - SerialException")
                    self.__devices[self.__masterDeviceName]["serial"] = None
                    self.__connected = False
                    raise se
                else:
                    data_from_device = data_from_device + received_character

            except Exception as e:
                self.log_exception(e)
                time.sleep(1)
                break
        
        try:
            if data_from_device is not b'' or data_from_device is not '' :
                self.__dataFromDevice = data_from_device
                self.__receivedString = str(data_from_device.decode("utf-8")).split('#')
                log.info("NDU Serial receivedString: %s",  self.__receivedString)
                if len(self.__receivedString) > 1:
                    currentDevice = "" #@warn get command first element
                    if "SNR1" in self.__receivedString[1]:
                        if len(self.__receivedString) > 3:
                            currentDevice = self.__receivedString[2]
                            log.info("NDU Serial SNR#OK: %s",  self.__receivedString)
                            self.sendData2Server(2, currentDevice)
                            if self.__isLightningOn:
                                #checkLuminetLightValue(self, isRule, isMove, isAmbientLight, dimPower)
                                self.__setDimPower = self.__ruleInstance.checkLuminetLightValue(self.__isRuleControlEnable, True, False, self.__setDimPower)
                                device_config = self.__devices[currentDevice].get("device_config")
                                requests = device_config["attributeUpdates"]
                                setCommand = self.setAttributeFiproDevice(currentDevice, requests, True)
                                self.sendData2Device(setCommand)
                                
                                time.sleep(3)
    
                                self.__setDimPower = self.__ruleInstance.CurrentDimPower()
                                setCommand = self.setAttributeFiproDevice(currentDevice, requests, True)
                                self.sendData2Device(setCommand)

                        log.info("NDU Serial #SNR: dimPower %s lightning %s ruleEnable %s", self.__setDimPower, self.__isLightningOn, self.__isRuleControlEnable)
                        
                    elif "ERR" in self.__receivedString[1]:
                        log.info("NDU Serial #ERR: %s")
                    elif "SNO" in self.__receivedString[1]:
                        if len(self.__setAttributeQuery) > 0:
                            self.__setAttributeQuery.pop(0)
                            log.info("NDU Serial #OK: %s", self.__receivedString)
                            log.info("NDU Serial #query list: %s", self.__setAttributeQuery)
                    elif "CLOSE" in self.__receivedString[1]:
                        log.info("SHUTDOWN")
                        subprocess.Popen(['shutdown','-h','now'])
            else:
                log.info("NDU Serial null: %s")
        
        except Exception as e:
            self.log_exception(e)
            self.close()
            # raise e

    def serialCommunicator(self, command, comm, readAgain = 1):
        # log.info("NDU Serial currentDevice: %s", currentDevice)
        log.info("NDU - writable command: %s", command)
        comm.write(str(command).encode('UTF-8'))
        self.__dataFromDevice = ""
        self.__receivedString = ""
        for i in range(readAgain):
            received_character = b''
            data_from_device = b''
            readCounter = 0
            while received_character != b'\n':  # We will read until receive LF symbol
                try:
                    if comm is None or not hasattr(comm, 'read'):
                        # log.debug("NDU - port is not ready..")
                        self.__connected = False
                        break
                    try:
                        received_character = comm.read(1)    # Read one symbol per time
                        if received_character.decode("utf-8") == '':
                            readCounter += 1
                        if readCounter > 2:
                            break
                    except AttributeError as e:
                        log.debug("NDU - AttributeError")
                        if comm is None:
                            self.__devices[self.__masterDeviceName]["serial"] = None
                            self.__connected = False
                            raise e
                    except serial.serialutil.SerialException as se:
                        log.debug("NDU - SerialException")
                        self.__devices[self.__masterDeviceName]["serial"] = None
                        self.__connected = False
                        raise se
                    else:
                        data_from_device = data_from_device + received_character

                except Exception as e:
                    self.log_exception(e)
                    time.sleep(1)
                    break
            
            try:
                if data_from_device != b'' or data_from_device != '' :
                    self.__dataFromDevice = data_from_device
                    self.__receivedString = str(data_from_device.decode("utf-8")).split('#')
                    if self.__receivedString is not ' ':
                        log.info("NDU Serial receivedString: %s",  self.__receivedString)
                    if len(self.__receivedString) > 1:
                        currentDevice = "" #@warn get command first element
                        if "SNR1" in self.__receivedString[1]:
                            if len(self.__receivedString) > 3:
                                currentDevice = self.__receivedString[2]
                                log.info("NDU Serial SNR#OK: %s",  self.__receivedString)
                                #self.sendData2Server(2, currentDevice)

                            log.info("NDU Serial #SNR: dimPower %s lightning %s ruleEnable %s", self.__setDimPower, self.__isLightningOn, self.__isRuleControlEnable)
                            
                        elif "ERR" in self.__receivedString[1]:
                            log.info("NDU Serial #ERR: %s", command)
                        elif "SNO" in self.__receivedString[1]:
                            if len(self.__setAttributeQuery) > 0:
                                self.__setAttributeQuery.pop(0)
                                log.info("NDU Serial #OK: %s", self.__receivedString)
                                log.info("NDU Serial #query list: %s", self.__setAttributeQuery)
                        else:
                            if len(self.__receivedString) > 3:
                                currentDevice = self.__receivedString[1]
                                self.sendData2Server(1, currentDevice)

                    elif len(self.__receivedString) > 0 and readAgain < 10:
                        if "OK+" in self.__receivedString[0]:
                            log.info("NDU - antenna config: %s", self.__receivedString[0])
                            if "OK+FU3" in self.__receivedString[0]:
                                self.__antennaConfigFinish = True
                                return
                            time.sleep(1)
                        else:
                            log.info("NDU - antenna config error: %s", self.__receivedString[0])
                            self.__setAttributeQuery.pop(0)
                else:
                    log.info("NDU Serial null: %s", command)
                    self.__setAttributeQuery.pop(0)
            
            except Exception as e:
                self.log_exception(e)
                self.close()
                # raise e

    def setAntennaConfig(self):
        try:
            comm = self.__devices[self.__masterDeviceName]["serial"]
            log.debug("NDU antenna config set pin low")
            antennaSetPin = LED(17)
            antennaSetPin.off()
            time.sleep(2)
            self.serialCommunicator("AT+DEFAULT\r\n", comm, 2)
            time.sleep(2)
            self.serialCommunicator("AT+C070\r\n", comm, 2)
            time.sleep(2)
            self.serialCommunicator("AT+RX\r\n", comm, 7)
            time.sleep(2)
            while self.__antennaConfigFinish == False:
                log.debug("NDU wait antenna config ")
                time.sleep(1)

            log.debug("NDU antenna config set pin high")
            antennaSetPin.on()    
            time.sleep(1)
        except Exception as e:
            self.log_exception(e)

    def run(self):    # Main loop of thread
        #self.setAntennaConfig()
        #self.setRuleController()
        telemetryCounter = 270
        listenerCounter = 0
        deviceCounter = 0
        try:
            while True:
                # @warn read serial connections
                device_serial_port = self.__devices[self.__masterDeviceName]["serial"]
                if device_serial_port is None or not hasattr(device_serial_port, 'read'):
                    # log.debug("NDU - port is not ready..")
                    if (time.time() - self.lastConnectionCheck) > self.reconnect_interval:
                        log.debug(
                            'NDU - Try reconnect to device after %s second', self.reconnect_interval)
                        self.__connect_to_devices()
                        if self.reconnect_interval < 59:
                            self.reconnect_interval = 5 * self.reconnect_count
                    else:
                        time.sleep(2)
                    continue
                
                listenerCounter += 1
                #log.debug("NDU - TELEMETRY COUNTER %s", str(telemetryCounter))
                
                if len(self.__setAttributeQuery) > 0:
                    setCommand = self.__setAttributeQuery[0]
                    if setCommand != "":
                        self.sendData2Device(setCommand)
                        time.sleep(1)
                        continue
                else:
                    time.sleep(1)
                    telemetryCounter += 1
                    #log.debug("NDU - TELEMETRY COUNTER %s", str(telemetryCounter))
                        
                    if(telemetryCounter > 300):
                        telemetryCounter = 0
                        self.serialCommunicator("#SNDALL", device_serial_port, readAgain=len(self.__devices) * 3)
                        # #time.sleep(10)
                        # device = self.__devices[deviceCounter]
                        # #self.sendData2Device("#" + str(device) + "#SND")
                        # device_config = self.__devices[device].get("device_config")
                        # localname = device_config["localName"]
                        # #log.debug("NDU - device local name %s", localname)
                        # checksum = 6712 + int(localname)
                        # #sendCommand = "#SNDALL"
                        # sendCommand = "#V1000#V2000#V3000#V4000#V5000#V6000#V7000#V8000#V9000#V10000#V11000#V12000#V13000#V14" + str(localname) + "#V15000#V16000#V17000#V18000#V19000#CS" + str(checksum)
                        # #log.debug("NDU - device local set command %s", sendCommand)
                        # self.sendData2Device(sendCommand)
                        # time.sleep(30)
                        # deviceCounter += 1
                        # if deviceCounter > 6:
                        #     deviceCounter = 0
                    
                    if(listenerCounter > 21):
                        listenerCounter = 0
                        self.onlyListenDevice(device_serial_port)

                    

        except Exception as e:
            self.log_exception(e)

    def log_exception(self, e):
        if hasattr(e, 'message'):
            log.error(e.message)
        else:
            log.exception(e)

    def close(self):    # Close connect function, usually used if exception handled in gateway main loop or in connector main loop
        self.stopped = True
        self.__gateway.del_device(self.__devices[self.__masterDeviceName])
        if self.__devices[self.__masterDeviceName]['serial'].isOpen():
            self.__devices[self.__masterDeviceName]['serial'].close()

    # Function used for processing attribute update requests from ThingsBoard
    def on_attributes_update(self, content):
        device_name = content["device"]
        log.debug("NDU - on_attributes_update device : %s , content : %s", content, device_name)
        if self.__devices.get(device_name) is not None:
            log.debug("NDU - device : %s", self.__devices.get(device_name))
            device_config = self.__devices[device_name].get("device_config")
            if device_config is not None:
                log.debug("NDU - device_config %s", device_config)
                if device_config.get("attributeUpdates") is not None:
                    requests = device_config["attributeUpdates"]
                    for request in requests:
                        attribute = request.get("name")
                        # sadece config icerisinde olan attribute isimleri cihaza gonderiliyor
                        if attribute is not None and attribute in content["data"]:
                            log.debug("NDU - gelen attribute : %s", attribute)
                            try:
                                value = content["data"][attribute]
                                self.deviceLastAttributes[device_name][attribute] = value
                                # @warn saat eklenecek
                                # @warn string kontrol edilecek
                                time.sleep(.01)
                            except Exception as e:
                                self.log_exception(e)

                    log.debug("NDU - last device attributes %s", self.deviceLastAttributes[device_name])
                    if "FN" in device_name:
                        self.setAttributeFiproDevice(device_name, requests)
                    else:
                        self.setAttributeNduEdge(device_name, requests)

    def setAttributeFiproDevice(self, device_name, requests, isSelfTrigged = False):
        try:
            allAttributesAvailable = True
            getTelemetryString = False
            for request in requests:
                attribute = request.get("name")
                if "LEDCONTROL1" in attribute:
                    self.__isRuleControlEnable  = self.deviceLastAttributes[device_name][attribute]
                    log.debug("NDU - last device attributes %s", self.__isRuleControlEnable)
                if "LEDCONTROL2" in attribute:
                    self.__isLightningOn = self.deviceLastAttributes[device_name][attribute]
                    log.debug("NDU - last device attributes %s", self.__isLightningOn)
                if "DIM_9" in attribute and isSelfTrigged == False:
                    self.__setDimPower = self.deviceLastAttributes[device_name][attribute]
                    log.debug("NDU - last device attributes %s", self.__setDimPower)
                if "TIMERCONTROL" in attribute:
                    self.__timerControl = self.deviceLastAttributes[device_name][attribute]
                    log.debug("NDU - last device attributes %s", self.__timerControl)
                if "VAL2" in attribute:
                    value = self.deviceLastAttributes[device_name][attribute]
                    if value > 0:
                        getTelemetryString = True
                    log.debug("NDU - last device attributes %s", getTelemetryString)

                if attribute in self.deviceLastAttributes[device_name]:
                    allAttributesAvailable = True
                else:
                    allAttributesAvailable = False
                    break
            
            isUseDefaultValue = True
            strAllSend = ""# '#' + device_name
            ledControlStr = "#V1"
            timControlStr = ["V2111","",""]
            dimControlStr = ["V5111","","",""]
            batControlStr = ["V9000","","",""]

            if self.__isRuleControlEnable and self.__isLightningOn:
                ledControlStr = "#V1000"
            
            elif self.__isLightningOn:
                ledControlStr = "#V1110"
                isUseDefaultValue = False
            else:
                ledControlStr = "#V1100"
                
            if self.__timerControl or getTelemetryString:
                ledControlStr = "#V1000"
                timControlStr[0] = "#V2000"
                dimControlStr[0] = "#V5000"
                batControlStr[0] = "#V9000"

            currentConverter = self.__devices[device_name]['converter']
            if allAttributesAvailable:
                for attribute2 in requests:
                    attName = attribute2.get("name")
                    value = self.deviceLastAttributes[device_name][attName]
                    str_to_send2 = ""
            
                    if "LEDCONTROL" in attName:
                        if "LEDCONTROL2" in attName:
                            str_to_send2 += ledControlStr

                        log.debug("NDU - led Control String %s : %s ctrl %s", attName, value, str_to_send2)

                    elif "TIM" in attName:
                        if self.__timerControl or getTelemetryString:
                            value = 0
                        if "TIM_7" in attName:
                            if isUseDefaultValue:
                                value = self.__defaultFiproValue[0]
                            
                            timControlStr[1] = attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithTrioFormat(value)))
                                            
                        if "TIM_8" in attName: 
                            if isUseDefaultValue:
                                value = self.__defaultFiproValue[1]
                            timControlStr[2] = attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithTrioFormat(value)))
                            for tmpStr in timControlStr:
                                str_to_send2 += "#" 
                                str_to_send2 += tmpStr
                                
                        log.debug("NDU - tim Control String %s : %s ctrl %s", attName, value, str_to_send2)

                    elif "DIM" in attName:
                        if self.__timerControl or getTelemetryString:
                            value = 0
                        if "DIM_7" in attName:
                            value = self.__setDimPower
                            if isUseDefaultValue:
                                value = self.__defaultFiproValue[2]
                            dimControlStr[1] = attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithTrioFormat(value)))
                        
                        if "DIM_8" in attName: 
                            value = self.__setDimPower
                            if isUseDefaultValue:
                                value = self.__defaultFiproValue[3]
                            dimControlStr[2] = attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithTrioFormat(value)))
                        
                        if "DIM_9" in attName:
                            value = self.__setDimPower
                            if isUseDefaultValue:
                                value = self.__defaultFiproValue[4]
                            dimControlStr[3] = attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithTrioFormat(value)))
                            for tmpStr in dimControlStr:
                                str_to_send2 += "#" 
                                str_to_send2 += tmpStr

                        log.debug("NDU - dim Control String %s : %s ctrl %s", attName, value, str_to_send2)

                    elif "BAT" in attName or "UVL" in attName:
                        if self.__timerControl or getTelemetryString:
                            value = 0
                        if "BATDOWN" in attName:
                            batControlStr[1] = attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithQuatroFormat(value)))
                        
                        elif "BATUP" in attName: 
                            batControlStr[2] = attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithQuatroFormat(value)))
                        
                        elif "UVL0" in attName:
                            batControlStr[3] = attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithQuatroFormat(value)))
                            for tmpStr in batControlStr:
                                str_to_send2 += "#" 
                                str_to_send2 += tmpStr
                    
                    elif "VAL" in attName:
                        if "VAL5" in attName:
                            if self.__timerControl is not True:
                                value = 0
                        if "VAL2" in attName:
                            if getTelemetryString is not True:
                                value = 0
                        str_to_send2 += "#" 
                        str_to_send2 += attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithTrioFormat(value)))
                        
                        log.debug("NDU - bat Control String %s : %s ctrl %s", attName, value, str_to_send2)
                    elif "V18" in attName or "V19" in attName:
                        if self.__timerControl is not True:
                            value = 0
                        str_to_send2 += "#" 
                        str_to_send2 += attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithTrioFormat(value)))
                        

                    strAllSend = strAllSend + str_to_send2
                    log.debug("NDU - strAllSend %s", strAllSend)

                #if len(self.__setAttributeQuery) < 2000:
                strAllSend += '#CS'
                strAddChecksum = currentConverter.checksum_calculator(strAllSend, False)
                if isSelfTrigged:
                    return str(strAddChecksum)

                self.__setAttributeQuery.append(str(strAddChecksum))
                log.debug("Fipro - Attribute update request to device %s : %s", device_name, str(strAddChecksum))

        except Exception as e:
            self.log_exception(e)

    def setAttributeNduEdge(self, device_name, requests):
        try:
            allAttributesAvailable = True

            for request in requests:
                attribute = request.get("name")
                #log.debug("NDU - last device attributes %s", attribute)
                if attribute in self.deviceLastAttributes[device_name]:
                    allAttributesAvailable = True
                else:
                    allAttributesAvailable = False
                    break

            strAllSend = '#' + device_name
            controlStr = "#CTR000"
            currentConverter = self.__devices[device_name]['converter']
            if allAttributesAvailable:
                for attribute2 in requests:
                    attName = attribute2.get("name")
                    value = self.deviceLastAttributes[device_name][attName]
                    if "Control" in attName:
                        str_to_send2 = ""
                    else: 
                        str_to_send2 = "#"
                    if "Control" in attName:
                        controlStr = currentConverter.setControlString(attName, value, controlStr)
                        log.debug("NDU - Set Control String %s : %s ctrl %s", attName, value, controlStr)

                    elif "Power" in attName:
                        str_to_send2 += attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setWithTrioFormat(value)))

                    elif "Time" in attName:
                        str_to_send2 += attribute2["stringToDevice"].replace("${" + attName + "}", str(currentConverter.setTimeWithFormat(19,0)))#int(value[0:2]), int(value[2,4]))))

                    else:
                        str_to_send2 += attribute2["stringToDevice"].replace("${" + attName + "}", str(value))

                    strAllSend += str_to_send2
                    #log.debug("NDU - strAllSend %s", strAllSend)

                if len(self.__setAttributeQuery) < 2000:
                    # @warn
                    strAllSend += controlStr
                    now = datetime.datetime.now()
                    strAllSend += '#CLK'
                    strAllSend += currentConverter.setTimeWithFormat(now.hour, now.minute)
                    strAllSend += '#CS'
                    strAddChecksum = currentConverter.checksum_calculator(strAllSend, False)
                    
                    self.__setAttributeQuery.append(str(strAddChecksum))
                    #self.__setAttributeQuery[device_name][0] = str(strAddChecksum)
                    log.debug("NDU - Attribute update request to device %s : %s", device_name, str(strAddChecksum))

        except Exception as e:
            self.log_exception(e)

    def server_side_rpc_handler(self, content):
        # log.info("RPC KOMUT GELDİ %s",content)
        if content is not None and content["device"] is not None:
            if content["data"] is None or content["data"].get("method") is None:
                log.debug(
                    "NDU - rpc data or method not found in content : %s", content)
                pass

            device_name = content["device"]
            data = content["data"]
            method = content["data"].get("method")
            # log.debug("NDU - rpc method : %s", method)
            # log.debug("NDU - rpc data : %s", data)

            if self.__devices.get(device_name) is not None:
                device_config = self.__devices[device_name].get(
                    "device_config")
                if device_config is not None and device_config.get("capabilities") is not None:
                    capabilities = device_config.get("capabilities")
                    found = False
                    for capability in capabilities:
                        if method == capability["method"]:
                            self.run_capability(device_name, capability, data)
                            found = True
                    if not found:
                        log.debug("NDU - capability not found : %s", method)
                else:
                    log.debug("NDU - device capabilities not found.")

    # data > {'id': 28, 'method': 'setLedStatus', 'params': {'status': 0}}
    # capability > {'method': 'setLedStatus', 'description': 'Set led status', 'stringToDevice': 'L1=${status}\n', 'singleParams': False, 'parameters': [{'name': 'status', 'type': 'number'}]}
    def run_capability(self, device_name, capability, data):
        try:
            stringToDevice = capability["stringToDevice"]  # check if exist

            log.debug("NDU - capability will run : %s", capability)
            if capability.get("parameters", None) is None:
                if capability.get("singleParams", False):
                    # log.debug("NDU - running singleparams capability")
                    stringToDevice = stringToDevice.replace(
                        "${" + "params" + "}", str(data["params"]))
                # elif capability.get("singleParams", True): # do nothing
                #     log.debug("NDU - running no paramter capability")
            else:
                for capability_param in capability["parameters"]:
                    for param in data["params"]:
                        # log.debug("NDU - gelen param : %s, capability_param.name : %s", param, capability_param["name"])
                        # log.debug("NDU - gelen type(param) : %s, type(capability_param.name) : %s", type(param), type(capability_param["name"]))
                        if str(capability_param["name"]) == str(param):
                            replace_expression = "${" + \
                                capability_param["name"] + "}"
                            log.debug("NDU - replace_expression %s with %s",
                                      replace_expression, str(data["params"].get(param)))
                            stringToDevice = stringToDevice.replace(
                                replace_expression, str(data["params"].get(param)))

            # log.debug("NDU - stringToDevice : %s", stringToDevice)
            # checksum eklenecek
            str_to_send = str(stringToDevice).encode("UTF-8")
            self.__devices[device_name]["serial"].write(str_to_send)

        except Exception as e:
            self.log_exception(e)

