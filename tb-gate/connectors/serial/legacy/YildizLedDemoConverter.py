from thingsboard_gateway.connectors.converter import Converter, log
# update by agit 4


class YildizLedDemoConverter(Converter):
    def __init__(self, config):
        self.__config = config
        self.result_dict = {
            'deviceName': config.get('name', 'CustomSerialDevice'),
            'deviceType': config.get('deviceType', 'default'),
            'attributes': [],
            'telemetry': []
        }

    def convert(self, config, data: bytes):
        # log.debug('CustomSerialUplinkConverter called')
        self.result_dict['attributes'] = []
        self.result_dict['telemetry'] = []
        self.result_dict['deviceName'] = config['name']
        currentDevice = config['name']
        #log.debug("NDU - config : %s", config)
        #log.debug("NDU - device name : %s", currentDevice)
        try:
            telemDelimeter = '#'  # @warn configden alınacak sonra
            telemData = data.split(telemDelimeter.encode('UTF-8'))
            log.debug("NDU - telemetry : %s", telemData)
            # @warn logs.conf
            # if str(telemData[1].decode("UTF-8")) != currentDevice:
            #     log.info("NDU wrong device : %s", str(telemData[1].decode("UTF-8")))
            #     return self.result_dict
            # @warn arduino debug
            # if telemData[0].decode('UTF-8') == 'd' or telemData[0].decode('UTF-8') == 'D':
            self.result_dict = self.parse_data(telemData, data)
            # else:
            #log.debug('%s LOG DATA : %s', config.get('name', 'CustomSerialDevice'), data.decode('UTF-8'))

            # @warn show dictionary
            #log.debug('result_dict : %s',  self.result_dict)

        except Exception as e:
            log.error("Error parsing data")
            log.exception(e)

        #log.info("NDU result dict: %s ", self.result_dict)
        return self.result_dict

    def parse_data(self, telemData, originData):
        #telemData.append(telemData[0]) #snr data
        log.debug("NDU get moving detector: %s", telemData)
        #log.debug("NDU get origin data: %s", originData)

        for telemetry in self.__config.get('telemetry'):
            try:
                data_to_convert = b''
                index = telemetry.get('index', -1)
                if index != -1:
                    dataType = telemetry.get('type', 'string')
                    # @warn checksum gönderilmeyecek
                    is_checksum = telemetry.get('isCheckSum', False)
                    isAttribute = telemetry.get('isAttribute', False)
                    is_Dynamic = telemetry.get('isDynamic', False)

                    if is_Dynamic:
                        if isDetectMoving:
                            data_to_convert = b'1'
                        else:
                            data_to_convert = b'0'
                    else:
                        data_to_convert = telemData[index + 1]  # 0 null, 1 deviceID
                    #log.info("NDU - data_to_convert: %s", data_to_convert)
                    

                    if dataType == 'float':
                        converted_data = {telemetry['key']: float(data_to_convert.decode('UTF-8'))}
                    elif dataType == 'int':
                        converted_data = {telemetry['key']: int(data_to_convert.decode('UTF-8'))}
                    else:
                        converted_data = {telemetry['key']: data_to_convert.decode('UTF-8')}

                    if not is_checksum:
                        if isAttribute:
                            self.result_dict['attributes'].append(converted_data)
                        else:
                            self.result_dict['telemetry'].append(converted_data)
                    else:
                        telemDataPure = str(originData.decode('UTF-8'))
                        csIndex = telemDataPure.find('#', len(telemDataPure) - 7, len(telemDataPure))  # -9 #XXXXXX\r\n
                        csIndex += 1
                        log.debug("NDU checksum Index %s", csIndex)
                        # +1 add must last '#'
                        if not self.chekcsum_validator(originData, int(data_to_convert.decode('UTF-8')), csIndex):
                            self.result_dict['telemetry'] = []
                            self.result_dict['attributes'] = []
                
            except Exception as e:
                log.exception(e)
        return self.result_dict

    def chekcsum_validator(self, data, csVal, csIndex):
        sum = 0
        for i in range(csIndex):
            sum += data[i]

        if sum == csVal:
            return True
        else:
            log.error(data)
            log.error("CHECKSUM FALSE len = %s -%s != %s index: %s \n %s",
                      data, sum, csVal, csIndex, len(data))
            return False

    def checksum_calculator(self, data, isFipro):
        sum = 0
        for i in range(len(data)):
            sum += ord(data[i])

        if isFipro:
            data += str('{:06d}'.format(sum))
        else:
            data += str('{:04d}'.format(sum))
        #log.info("CHECKSUM len = -%s -%s \n%s", data, sum, len(data))

        return data

    def setControlString(self, attName, value, controlString):
        cStr = list(controlString)
        tempValue = 0
        if value == True:
            tempValue = 1
        else:
            tempValue = 0
        if "RuleControl" in attName:
            cStr[4] = str(tempValue)
        if "LightControl" in attName:
            cStr[5] = str(tempValue)
        if "MoveControl" in attName:
            cStr[6] = str(tempValue)
        return "".join(cStr)

    def setFiproControlString(self, attName, value, controlString):
        cStr = list(controlString)
        tempValue = 0
        if value == True:
            tempValue = 1
        else:
            tempValue = 0
        if "1" in attName:
            cStr[2] = str(tempValue)
        if "2" in attName:
            cStr[3] = str(tempValue)
        if "3" in attName:
            cStr[4] = str(tempValue)
        return "".join(cStr)

    def setTimeWithFormat(self, hour, minute):

        clkStr = str('{:02d}'.format(hour))
        clkStr += str('{:02d}'.format(minute))
        return clkStr

    def setWithQuatroFormat(self, strValue):
        value = int(strValue)
        if value > 9999:
            value = 9999
        if value < 0:
            value = 0
        return str('{:04d}'.format(value))
    
    def setWithTrioFormat(self, strValue):
        value = int(strValue)
        if value > 999:
            value = 999
        if value < 0:
            value = 0
        return str('{:03d}'.format(value))
