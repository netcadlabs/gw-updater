"""
Serial olarak gateway'e bağlanmış olan arduino ile bağlantı kurmaya
yarayan örnek connector.

connector ayar dosyası : etc/thingsboard-gateway/config/custom_serial_ardunio_kablo.json
connector'un kullandigi converter (ayar dosyasında belirtilen) :  CustomArduinoSerialConverter (custom_arduino_serial_converter.py)
"""

import time
from threading import Thread
from random import choice
from string import ascii_lowercase

import serial

from thingsboard_gateway.connectors.connector import Connector, log    # Import base class for connector and logger
from thingsboard_gateway.tb_utility.tb_utility import TBUtility


class CustomArduinoSerialConnector(Thread, Connector):    # Define a connector class, it should inherit from "Connector" class.
    def __init__(self, gateway, config, connector_type):
        super().__init__()    # Initialize parents classes
        self.statistics = {'MessagesReceived': 0,
                           'MessagesSent': 0}    # Dictionary, will save information about count received and sent messages.
        self.__config = config    # Save configuration from the configuration file.
        log.debug("NDU - config %s", config)
        self.__gateway = gateway    # Save gateway object, we will use some gateway methods for adding devices and saving data from them.
        self.setName(self.__config.get("name",
                                       "Custom %s connector " % self.get_name() + ''.join(choice(ascii_lowercase) for _ in range(5))))    # get from the configuration or create name for logs.
        log.info("Starting Custom %s connector", self.get_name())    # Send message to logger
        self.daemon = True    # Set self thread as daemon
        self.stopped = True    # Service variable for check state
        self.__connected = False    # Service variable for check connection to device
        self.__devices = {}    # Dictionary with devices, will contain devices configurations, converters for devices and serial port objects
        self.__load_converters(connector_type)    # Call function to load converters and save it into devices dictionary
        self.lastConnectionCheck = 0
        self.reconnect_interval = 5 # first interval(seconds) to reconnect device
        self.reconnect_count = 0 # number of reconnection attempt
        self.__connect_to_devices()    # Call function for connect to devices
        log.info('Custom connector %s initialization success.', self.get_name())    # Message to logger
        log.info("Devices in configuration file found: %s ", '\n'.join(device for device in self.__devices))    # Message to logger

    def __connect_to_devices(self):    # Function for opening connection and connecting to devices
        for device in self.__devices:
            device_config = self.__devices[device]['device_config']
            try:    # Start error handler
                connection_start = time.time()
                
                # log.debug("NDU - for device_config %s", device_config)
                if self.__devices[device].get("serial") is None \
                        or self.__devices[device]["serial"] is None \
                        or not self.__devices[device]["serial"].isOpen():    # Connect only if serial not available earlier or it is closed.
                    self.__devices[device]["serial"] = None
                    
                    while self.__devices[device]["serial"] is None or not self.__devices[device]["serial"].isOpen():    # Try connect
                        # connection to serial port with parameters from configuration file or default
                        # log.debug("NDU - for device %s", device_config)
                        self.reconnect_count = self.reconnect_count + 1
                        log.debug("NDU - (%s) trying to serial connection for %s",self.reconnect_count, device_config["name"])
                        
                        self.__devices[device]["serial"] = serial.Serial(port=device_config.get('port', '/dev/ttyUSB0'),
                                                                         baudrate=device_config.get('baudrate', 9600),
                                                                         bytesize=device_config.get('bytesize', serial.EIGHTBITS),
                                                                         parity=device_config.get('parity', serial.PARITY_NONE),
                                                                         stopbits=device_config.get('stopbits', serial.STOPBITS_ONE),
                                                                         timeout=device_config.get('timeout', 1),
                                                                         xonxoff=device_config.get('xonxoff', False),
                                                                         rtscts=device_config.get('rtscts', False),
                                                                         write_timeout=device_config.get('write_timeout', None),
                                                                         dsrdtr=device_config.get('dsrdtr', False),
                                                                         inter_byte_timeout=device_config.get('inter_byte_timeout', None),
                                                                         exclusive=device_config.get('exclusive', None))
                        time.sleep(.1)
                        if time.time() - connection_start > 100:    # Break connection try if it setting up for 10 seconds
                            log.error("Connection refused per timeout for device %s", device_config.get("name"))
                            break
                else:
                    log.debug("NDU - already connected and serial port is open for %s", device_config.get("name"))
            except serial.serialutil.SerialException:
                log.error("Port %s for device %s - not found", device_config.get('port', '/dev/ttyUSB0'), device)
            except Exception as e:
                log.exception(e)
            else:    # if no exception handled - add device and change connection state
                log.debug("NDU - serial connection established for %s", device_config["name"])
                self.__gateway.add_device(device_config["name"], {"connector": self}, device_type=device_config["type"])
                self.__connected = True
                self.reconnect_count = 0
        
        self.lastConnectionCheck = time.time()

    def open(self):    # Function called by gateway on start
        self.stopped = False
        self.start()

    def get_name(self):    # Function used for logging, sending data and statistic
        return self.name

    def is_connected(self):    # Function for checking connection state
        return self.__connected

    def __load_converters(self, connector_type):    # Function for search a converter and save it.
        devices_config = self.__config.get('devices')
        try:
            if devices_config is not None:
                for device_config in devices_config:
                    if device_config.get('converter') is not None:
                        converter = TBUtility.check_and_import(connector_type, device_config['converter'])
                        self.__devices[device_config['name']] = {'converter': converter(device_config),
                                                                 'device_config': device_config}
                        log.debug("NDU - __load_converters converter %s loaded with : %s", device_config['converter'], device_config)
                    else:
                        log.error('Converter configuration for the custom connector %s -- not found, please check your configuration file.', self.get_name())
            else:
                log.error('Section "devices" in the configuration not found. A custom connector %s has being stopped.', self.get_name())
                self.close()
        except Exception as e:
            log.exception(e)

    def run(self):    # Main loop of thread
        try:
            while True:
                for device in self.__devices:
                    device_serial_port = self.__devices[device]["serial"]
                    received_character = b''
                    data_from_device = b''
                    while received_character != b'\n':  # We will read until receive LF symbol
                        try:
                            if device_serial_port is None or not hasattr(device_serial_port, 'read'):
                                # log.debug("NDU - port is not ready..")
                                self.__connected = False
                                break

                            try:
                                received_character = device_serial_port.read(1)    # Read one symbol per time
                            except AttributeError as e:
                                log.debug("NDU - AttributeError")
                                if device_serial_port is None:
                                    self.__devices[device]["serial"] = None
                                    self.__connected = False
                                    raise e
                            except serial.serialutil.SerialException as se:
                                log.debug("NDU - SerialException")
                                self.__devices[device]["serial"] = None
                                self.__connected = False
                                raise se
                            else:
                                data_from_device = data_from_device + received_character

                        except Exception as e:
                            self.log_exception(e)
                            time.sleep(1)
                            break

                    try:
                        log.debug("NDU %s",data_from_device)
                        if data_from_device is not b'' or data_from_device is not b'#BEGIN\r\n':
                            converted_data = self.__devices[device]['converter'].convert(self.__devices[device]['device_config'], data_from_device)
                            log.debug("NDU %s",converted_data)
                            self.__gateway.send_to_storage(self.get_name(), converted_data)
                            # log.debug("NDU - converted_data %s", converted_data)
                            time.sleep(.1)
                    except Exception as e:
                        self.log_exception(e)
                        # self.close()
                        # raise e

                if not self.__connected:
                    if (time.time() - self.lastConnectionCheck) > self.reconnect_interval: 
                        log.debug('NDU - Try reconnect to device after %s second', self.reconnect_interval)
                        self.__connect_to_devices() 
                        if self.reconnect_interval < 59:
                            self.reconnect_interval = 5 * self.reconnect_count
                    else:
                        time.sleep(2)
                    
        except Exception as e:
            self.log_exception(e)

    def log_exception(self, e):
        if hasattr(e, 'message'):
            log.error(e.message)
        else:
            log.exception(e)

    def close(self):    # Close connect function, usually used if exception handled in gateway main loop or in connector main loop
        self.stopped = True
        for device in self.__devices:
            self.__gateway.del_device(self.__devices[device])
            if self.__devices[device]['serial'].isOpen():
                self.__devices[device]['serial'].close()

    def on_attributes_update(self, content):    # Function used for processing attribute update requests from ThingsBoard
        device_name = content["device"]
        log.debug("NDU - on_attributes_update device : %s , content : %s", content , device_name)
        if self.__devices.get(device_name) is not None:
            log.debug("NDU - device : %s", self.__devices.get(device_name))
            device_config = self.__devices[device_name].get("device_config")
            if device_config is not None:
                # log.debug("NDU - device_config %s", device_config)
                if device_config.get("attributeUpdates") is not None:
                    requests = device_config["attributeUpdates"]
                    for request in requests:
                        attribute = request.get("name")
                        # sadece config icerisinde olan attribute isimleri cihaza gonderiliyor
                        if attribute is not None and attribute in content["data"]:
                            # log.debug("NDU - gelen attribute : %s", attribute)
                            try:
                                value = content["data"][attribute]
                                str_to_send = str(request["stringToDevice"].replace("${" + attribute + "}", str(value))).encode("UTF-8")
                                if self.__devices[device_name]["serial"] is not None:
                                    self.__devices[device_name]["serial"].write(str_to_send)
                                    # log.debug("NDU - Attribute update request to device %s : %s", device_name, str_to_send)
                                    time.sleep(.01)
                            except Exception as e:
                                self.log_exception(e)

    def server_side_rpc_handler(self, content):
        # log.info("RPC KOMUT GELDİ %s",content)
        if content is not None and content["device"] is not None:
            if content["data"] is None or content["data"].get("method") is None:
                log.debug("NDU - rpc data or method not found in content : %s", content)
                pass

            device_name = content["device"]
            data = content["data"]
            method = content["data"].get("method");
            # log.debug("NDU - rpc method : %s", method)
            # log.debug("NDU - rpc data : %s", data)

            if self.__devices.get(device_name) is not None:
                device_config = self.__devices[device_name].get("device_config")
                if device_config is not None and device_config.get("capabilities") is not None:
                    capabilities = device_config.get("capabilities")
                    found = False
                    for capability in capabilities:
                        if method == capability["method"]:
                            self.run_capability(device_name, capability, data)
                            found = True
                    if not found:
                        log.debug("NDU - capability not found : %s", method)
                else:
                    log.debug("NDU - device capabilities not found.")

    # data > {'id': 28, 'method': 'setLedStatus', 'params': {'status': 0}}
    # capability > {'method': 'setLedStatus', 'description': 'Set led status', 'stringToDevice': 'L1=${status}\n', 'singleParams': False, 'parameters': [{'name': 'status', 'type': 'number'}]}
    def run_capability(self, device_name, capability, data):
        try:
            stringToDevice = capability.get("stringToDevice", None)
            device_response = None

            if stringToDevice is not None:
                log.debug("NDU - capability will run : %s", capability)
                log.debug("NDU - capability data : %s", data)
                if capability.get("parameters", None) is None:
                    if capability.get("singleParams", False):
                        log.debug("NDU - running singleparams capability")
                        stringToDevice = stringToDevice.replace("${" + "params" + "}" , str(data["params"]))
                    # elif capability.get("singleParams", True): # do nothing
                    #     log.debug("NDU - running no paramter capability")
                else:
                    for capability_param in capability["parameters"]:
                        for param in data["params"]:
                            log.debug("NDU - gelen param : %s, capability_param.name : %s", param, capability_param["name"])
                            log.debug("NDU - gelen type(param) : %s, type(capability_param.name) : %s", type(param), type(capability_param["name"]))
                            if str(capability_param["name"]) == str(param):
                                replace_expression = "${" + capability_param["name"] + "}"
                                log.debug("NDU - replace_expression %s with %s", replace_expression, str(data["params"].get(param)))
                                stringToDevice = stringToDevice.replace(replace_expression , str(data["params"].get(param)))

                log.debug("NDU - stringToDevice : %s", stringToDevice)
                str_to_send = str(stringToDevice).encode("UTF-8")

                if self.__devices[device_name]["serial"] is not None:
                    if capability.get("responseHandler") is None:
                        self.__devices[device_name]["serial"].write(str_to_send)
                    else:
                        device_response = self.read_from_device(device_name, str_to_send)
            
            response_body = capability.get("responseBody", None)

            if  capability.get("type") is not None and str(capability.get("type")) == str("twoway"):
                try:
                    if response_body is None:
                        self.__gateway.send_rpc_reply(device=device_name, req_id=data["id"], success_sent=True)
                    elif device_response is not None:
                        response_content = self.format_response(response_body, device_response, capability.get("responseHandler"))
                        self.__gateway.send_rpc_reply(device=device_name, req_id=data["id"], content=response_content)
                        log.debug("NDU - %s return response(1) : %s",capability["method"], response_content)
                    else:
                        response_content = response_body
                        self.__gateway.send_rpc_reply(device=device_name, req_id=data["id"], content=response_content)
                        log.debug("NDU - %s return response(2) : %s",capability["method"], response_content)


                except Exception as e:
                    self.log_exception(e)
            else:
                log.debug("NDU - capability is not twoway,  type is : %s", capability.get("type"))

        except Exception as e:
            self.log_exception(e)

    # cihaza str_to_send string degerini gonderirir ve karsılıgınca varsa veri okur ve bunu string olarak doner.
    def read_from_device(self, device_name, str_to_send):
        self.__devices[device_name]["serial"].write(str_to_send)
        return "response_from_device"

    def format_response(self, response_body, device_response, response_handler=None):
        response_content = response_body + device_response
        return response_content
