from thingsboard_gateway.connectors.converter import Converter, log

class CustomArduinoSerialConverter(Converter):
    def __init__(self, config):
        self.__config = config
        self.result_dict = {
            'deviceName': config.get('name', 'CustomSerialDevice'),
            'deviceType': config.get('deviceType', 'default'),
            'attributes': [],
            'telemetry': []
        }

    def convert(self, config, data: bytes):
        log.debug('CustomSerialUplinkConverter called')
        self.result_dict['attributes'] = []
        self.result_dict['telemetry'] = []

        try:
            telemDelimeter = ';'
            telemData = data.split(telemDelimeter.encode('UTF-8'))
            
            # log.info(telemData)

            if telemData[0].decode('UTF-8') == 'd' or telemData[0].decode('UTF-8') == 'D':
                self.result_dict = self.parse_data(telemData)
            else:
                log.debug('%s LOG DATA : %s', config.get('name', 'CustomSerialDevice'), data.decode('UTF-8'))
            
            log.debug('result_dict : %s',  self.result_dict)

        except Exception as e:
            log.error("Error parsing data")
            log.exception(e)

        return self.result_dict

    def parse_data(self, telemData):
        for config_object in self.__config.get('telemetry'):
            data_to_convert = ''
            
            index = config_object.get('index', -1)
            if index is not -1:
                data_to_convert = telemData[index + 1]
                # log.debug('data_to_convert : %s',  data_to_convert)
                dataType = config_object.get('type', 'string')

                if dataType == 'float':
                    converted_data = {config_object['key']: float(data_to_convert.decode('UTF-8'))}
                elif dataType == 'int':
                    converted_data = {config_object['key']: int(data_to_convert.decode('UTF-8'))}
                else:
                    converted_data = {config_object['key']: data_to_convert.decode('UTF-8')}
                
                self.result_dict['telemetry'].append(converted_data)
                # log.debug('data_to_convert : %s',  data_to_convert)
        
        return self.result_dict

    # def getValue