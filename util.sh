#!/bin/bash

# Aşağıdaki gibi kullanılabilir
# Logger "Log message"

Logger(){
    now_str=$(date +'%D %H:%M:%S')
	echo "$now_str - $1"
}


ClearLogs(){
    > /var/log/thinsboard-gateway/gw-updater.log
}