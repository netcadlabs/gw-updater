# The IP for the server you wish to ping (8.8.8.8 is a public Google DNS server)
SERVER=2.59.119.196

# Only send two pings, sending output to /dev/null
/bin/ping -c 2 '2.59.119.196' > /dev/null

# If the return code from ping ($?) is not 0 (meaning there was an error)
if [ $? -ge 1 ] ; then
    # Restart the wireless interface
    echo "Network down, fixing..."
    sudo ifconfig wlan0 down
    sudo ifconfig wlan0 up
    echo "wlan0 reconnected at `date`"
else
    echo "Network okey"
fi